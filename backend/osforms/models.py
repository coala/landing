from django.db import models
from org.models import Contributor


class OSFormRecord(models.Model):
    user = models.ForeignKey(Contributor, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, verbose_name='Title')
    description = models.TextField(max_length=1000, verbose_name='About form',
                                   null=True)
    url = models.URLField(verbose_name='Form link')
    expiry_date = models.DateTimeField(verbose_name='Expiry date', null=True)

    def __str__(self):
        return self.title
