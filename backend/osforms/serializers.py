from rest_framework import serializers


from osforms.models import OSFormRecord


class OSFormRecordsSerializer(serializers.ModelSerializer):

    class Meta:
        model = OSFormRecord
        fields = '__all__'

    def validate_user(self, username):
        user_teams = username.teams.all().count()
        if user_teams == 1:
            raise serializers.ValidationError(
                "{} isn't a member of developer team.".format(username.login))
        return username

    def validate_title(self, value):
        if len(value.strip()) < 10:
            raise serializers.ValidationError(
                'The form title should be more than 10 characters.'
            )
        return value.strip()

    def validate_description(self, value):
        return value.strip()
