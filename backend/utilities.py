import logging
from django.core.mail import send_mail

from coala_web.settings import EMAIL_HOST_USER, EMAIL_HOST_PASSWORD


def send_email(subject, user_email, message):
    """
    Send an e-mail to the user using Django EMAIL Backend
    :param subject: The email subject
    :param user_email: recipient email
    :param message: The email body message
    """
    if not EMAIL_HOST_USER:
        logging.error('Organization email not provided! Please set environment'
                      'variable ORG_EMAIL')
    if not EMAIL_HOST_PASSWORD:
        logging.error('Organization account password not provided! Please set'
                      'environment variable ORG_PASSWORD')
    if EMAIL_HOST_USER and EMAIL_HOST_PASSWORD:
        return send_mail(from_email=EMAIL_HOST_USER, subject=subject,
                         message=message, recipient_list=[user_email, ])
    exit()
