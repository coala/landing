from org.newcomers import active_newcomers
from .models import Participant


def create_gamification_participants():
    """
    Create participants which will be used in the gamification system.
    """
    for participant in active_newcomers():
        Participant.objects.get_or_create(username=participant)
