from rest_framework import generics

from .models import Participant
from .serializers import ParticipantSerializer


class GamificationList(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = ParticipantSerializer
    queryset = Participant.objects.all()
