from rest_framework import serializers

from .models import (
    Activity,
    Badge,
    BadgeActivity,
    Level,
    Participant
)


class ActivitySerializer(serializers.ModelSerializer):

    class Meta:
        model = Activity
        fields = '__all__'


class BadgeActivitySerializer(serializers.ModelSerializer):

    class Meta:
        model = BadgeActivity
        fields = '__all__'


class BadgeSerializer(serializers.ModelSerializer):
    b_activities = BadgeActivitySerializer(many=True)

    class Meta:
        model = Badge
        fields = '__all__'


class LevelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Level
        fields = '__all__'


class ParticipantSerializer(serializers.ModelSerializer):
    level = LevelSerializer(many=False)
    activities = ActivitySerializer(many=True)
    badges = BadgeSerializer(many=True)

    class Meta:
        model = Participant
        fields = '__all__'
