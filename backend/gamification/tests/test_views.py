from django.test import TestCase

from gamification.models import Participant


class GamificationViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Create 10 participants for the tests
        number_of_participants = 10
        for i in range(number_of_participants):
            Participant.objects.create(username='usertest'+str(i))

    def test_view_url_exists_at_desired_location(self):
        resp = self.client.get('/gamification/')
        self.assertEqual(resp.status_code, 200)
