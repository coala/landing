from django.test import TestCase

from gamification.labels import get_sorted_labels


class GetSortedLabelsTest(TestCase):

    def test_get_sorted_labels(self):
        labels_list = [
            {'name': 'difficulty/newcomer'},
            {'name': 'type/bug'},
            {'name': 'status/invalid'},
            {'name': 'status/duplicate'}
        ]
        sorted_labels_list = get_sorted_labels(labels_list)
        expected_labels_list = ['status/invalid']
        self.assertEquals(sorted_labels_list, expected_labels_list)

        labels_list.remove({'name': 'status/invalid'})
        sorted_labels_list = get_sorted_labels(labels_list)
        expected_labels_list = ['status/duplicate']
        self.assertEquals(sorted_labels_list, expected_labels_list)

        labels_list.remove({'name': 'status/duplicate'})
        sorted_labels_list = get_sorted_labels(labels_list)
        expected_labels_list = ['difficulty/newcomer', 'type/bug']
        self.assertEquals(sorted_labels_list, expected_labels_list)
