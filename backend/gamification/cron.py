from .create_participants import create_gamification_participants
from .update_participants import update_data


def run_gamification():
    create_gamification_participants()
    update_data()
