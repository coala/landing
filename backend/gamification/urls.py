from django.conf.urls import url

from .views import GamificationList

urlpatterns = [
    url(r'^$', GamificationList.as_view()),
]
