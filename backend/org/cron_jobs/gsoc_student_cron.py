import os
from functools import lru_cache
import logging

from github import Github
from github.GithubException import UnknownObjectException

from coala_web.settings import NETLIFY, ORG_NAME, GITHUB_API_KEY
from org.serializers import GSOCStudentSerializer
from org.models import Contributor
from org.git import get_org
from utilities import send_email


ORG = get_org()
github_obj = Github(GITHUB_API_KEY)


@lru_cache(maxsize=32)
def fetch_submissions():
    """
    Fetch all the form submissions of the contributors who have requested to
    add themselves into the database, saying that they are GSoC Students and
    have participated with the organization in previous years.
    """
    form_name = os.environ.get('GSOC_STUDENT_FORM_NAME')

    if not form_name:
        logging.error('GSoC Student form name not provided!'
                      'Please set environment variable'
                      ' GSOC_STUDENT_FORM_NAME.')
        exit()
    else:
        submissions = NETLIFY.get_submissions(form_name)
        # Validate whether the details provided by user are valid or not. If
        # not, send an email giving information about errors.
        for entry in submissions:
            user_ = entry['data']
            student_data = get_student_data(user_)
            gsoc_student = GSOCStudentSerializer(data=student_data)
            valid_student = check_user_submission(user_)

            if gsoc_student.is_valid() and valid_student:
                student = gsoc_student.save()
                # Send a verification email to mentors asking them to verify
                # the details of the student. Whether the details are correct
                # or not.
                mentors_email = get_mentors_email(student)
                send_validation_mail(student, mentors_email)
            else:
                logging.error('Submission {form_id} not saved for form'
                              '{form_name}.'.format(form_id=entry['id'],
                                                    form_name=form_name))
                prepare_and_send_to_user(gsoc_student.errors, student_data)

            NETLIFY.delete_submission(entry['id'])


def get_student_data(user_):
    """
    Get the submitted data by the user
    :param user_: The form entry data
    :return: A dict with all related information of the GSoC Student
    """
    return {
        'user': user_['user'],
        'year': user_['year'],
        'project_topic': user_['project_topic'],
        'project_desc': user_['project_desc'],
        'accepted_proposal': user_['accepted_proposal'],
        'cEP': user_['cEP'],
        'project_url': user_['project_url'],
        'coala_projects_mr': user_['add_project_mr'],
        'mentors': [mentor.strip() for mentor in user_['mentors'].split(',')],
        'image': user_['image']
    }


def check_user_submission(user_data):
    """
    Check whether the user form submission is valid or not
    :param user_data: GSoC Student data
    :return: A Boolean, whether the student is valid or not
    """
    cEP_merged = check_cep_status(user_data['cEP'])
    project_mr_valid = check_project_mr(user_data['coala_projects_mr'])
    return any([cEP_merged, project_mr_valid])


def check_cep_status(pull_request):
    """
    Check whether the cEP is merged or not
    :param pull_request: The web url of the cEP PR
    :return: A Boolean
    """
    cEPs_repo = ORG.get_repo('cEPs')
    pr_number = pull_request.split('/')[-1]
    try:
        return cEPs_repo.get_pull(pr_number).is_merged()
    except UnknownObjectException:
        return False


def check_project_mr(pull_request):
    """
    Check whether the PR, that adds the project to coala/projects repository,
     is having the label "Project Proposal" and is being accepted by
     maintainer or not
    :param pull_request: The web url of the PR
    :return: A Boolean
    """
    projects_repo = ORG.get_repo('projects')
    pr_number = pull_request.split('/')[-1]
    try:
        pr = projects_repo.get_pull(pr_number)
        if not pr.is_merged():
            accepted_by_maintainer = maintainer_accepted(pr)
            is_gsoc_project = has_label('Project Proposal', pr)
            return all([accepted_by_maintainer, is_gsoc_project])
        return True
    except UnknownObjectException:
        return False


def maintainer_accepted(pr):
    """
    Check whether the Pull Requested is being approved by maintainer or not
    :param pr: The IGitt PullRequest Instance
    :return: A Boolean
    """
    accepted_by_maintainer = False
    accepted_by = None
    for review in pr.get_reviews():
        if review.state == 'APPROVED':
            username = review.user.login
            contrib = Contributor.objects.filter(login=username)
            if contrib.exists():
                contrib = contrib.first()
                maintainers_team = '{} maintainers'.format(ORG_NAME)
                filter_teams = contrib.teams.filter(
                    name=maintainers_team
                )
                accepted_by_maintainer = filter_teams.exists()
        elif (accepted_by_maintainer and
              review.user.login == accepted_by):
            accepted_by_maintainer = False
    return accepted_by_maintainer


def has_label(label, pr):
    """
    Check whether the Pull Request has a given label or not
    :param label: The name of the label
    :param pr: The IGitt PullRequest instance
    :return: A Boolean
    """
    for label_obj in pr.labels:
        if label_obj.name == label:
            return True
    return False


def get_email(user):
    """
    Get the user email
    :param user: The GitHub Username
    :return: user-email
    """
    user_obj = github_obj.get_user(user)
    return user_obj.email


def get_mentors_email(student):
    """
    Get email of the mentors
    :param student: The GSoCStudentSerializer object
    :return: A dict of mentors email
    """
    emails = {}
    for mentor in student.mentors.all():
        emails[mentor.login] = get_email(mentor.login)
    return emails


def send_validation_mail(student, mentors):
    """
    Send a validation email to all the mentors whi mentored student during
    GSoC
    :param student: The GSoCStudent model instance
    :param mentors: a dict of mentors with their email address
    """
    username = student.user.login
    subject = '{}: Did you mentored {} in GSoC-{}?'.format(ORG_NAME, username,
                                                           student.year)
    message_body = (
        'Hi {mentor_login}!\nDid you mentored {username} in GSoC-{year}?\n'
        'Following are the project related details:\nProject Title: {title}'
        '\nDescription: {desc}\nAccepted Proposal: {proposal}\ncEP: {cEP}\n'
        'Project URL: {url}\n\nIf you mentored this project, please click the'
        ' below given link to validate the student, to successfully add the'
        ' student into the database. {validation_link}'
    )
    for mentor, email in mentors.items():
        message = message_body.format(
            mentor_login=mentor, username=username, year=student.year,
            title=student.project_topic, proposal=student.accepted_proposal,
            desc=student.project_desc, cEP=student.cEP,
            url=student.project_url,
            validation_link=get_validation_link(student)
        )
        send_email(subject, email, message)


def get_validation_link(student):
    """
    Get the link which will be used by mentors to validate whether the GSoC
    Student is valid or not
    :param student: The GSOCStudent Model instance
    :return: the validation link
    """
    return 'https://webservices.{org}.io/gsoc/students/{id}/save'.format(
        org=ORG_NAME, id=student.id
    )


def prepare_and_send_to_user(errors, user):
    """
    Prepare the email which is to be sent to user in case there are any
    errors in the form submission
    :param errors: A dict of errors
    :param user: The student data
    """
    user_email = get_email(user['user'])
    subject = '{}: GSoC Student form rejected!'.format(ORG_NAME)
    message_body = (
        'Hi {username}!\nThere were some errors in your GSoC Student form'
        ' submission.\nFollowing were the errors:\n{errors_info}\nPlease'
        ' correct the errors and re-fill the form.\nNote: Make sure to read'
        ' the hint(s) or placeholder(s), if any, while filling up the form.'
    )
    field_errors = {}
    for error_field, error in errors.items():
        error_log = '\n'.join(
            [
                '{index}. {info}'.format(index=error.index(e)+1, info=e)
                for e in error
            ]
        )
        field_errors[error_field] = error_log
    errors_info = '\n'.join(
        [
            '{field_name}: {err}'.format(field_name=k, err=v)
            for k, v in field_errors.items()
        ]
    )
    message = message_body.format(username=user['user'],
                                  errors_info=errors_info)
    send_email(subject, user_email, message)
