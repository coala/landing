import os
import datetime

import logging
from bs4 import BeautifulSoup
from functools import lru_cache

import requests


from coala_web.settings import NETLIFY
from org.serializers import CalendarEventsSerializer
from org.models import Contributor, CalendarEvent
from osforms.cron import prepare_and_send_email


@lru_cache(maxsize=32)
def fetch_calendar_events():
    """
    Fetch the calendar events submissions that the contributors requested.
    :return:
    """
    form_name = os.environ.get('CALENDAR_NETLIFY_FORM_NAME')

    if not form_name:
        logging.error('Calendar events netlify form name not provided! Please'
                      'environment variable CALENDAR_NETLIFY_FORM_NAME.')
        exit()

    submissions = NETLIFY.get_submissions(form_name)
    for entry in submissions:
        user_ = entry['data']
        event_data = {
            'user': user_['user'],
            'title': user_['title'],
            'description': user_['description'],
            'start_date_time': user_['start_date_time'],
            'end_date_time': user_['end_date_time'],
        }
        record = CalendarEventsSerializer(data=event_data)

        if record.is_valid():
            record.save()
        else:
            logging.error('Submission {form_id} not saved for form'
                          ' {form_name}.'.format(form_id=entry['id'],
                                                 form_name=form_name))
            user_objs = Contributor.objects.filter(login=user_['user'])
            if user_objs > 0:
                prepare_and_send_email(record, user_objs.first())

        NETLIFY.delete_submission(entry['id'])


@lru_cache(maxsize=32)
def save_gsoc_timeline():
    """
    Save the Google Summer of Code timeline to the database
    """
    timeline_table_rows, year = get_timeline_table_rows('gsoc')

    # A list of events which are not being saved during processing of the
    # scrapped data due to un-availability of exact date-time of the event
    # to be saved
    unsaved_events = []

    # Starting from index 1 as index 0 contains just the table headings,
    for tr in range(1, len(timeline_table_rows)):

        # Find all data in the table row
        table_cells_data = timeline_table_rows[tr].find_all('td')

        # First column of row contains either program year or the event
        # date-time. If it has a `strong` tag, then it is an year else it is
        # a date-time string
        year_table_entry = table_cells_data[0].find_all('strong')

        if len(year_table_entry) != 0:
            year = year_table_entry[0].get_text()
        else:
            event_time_period = table_cells_data[0].get_text()

            # Column 1 of the row contains the event description or say, title
            event = table_cells_data[1].get_text()

            month = None
            date = None
            time = None
            start_date_time = None
            end_date_time = None

            if (event_time_period != 'Community Bonding Period'
                    and event_time_period != 'Work Period'):
                # Event time period is of the following format:
                # Month Date Time UTC
                sub_periods = event_time_period.split(' ')

                if sub_periods.__contains__('UTC'):
                    sub_periods.remove('UTC')

                for period in sub_periods:
                    if period.isalpha():
                        month = get_month(period)
                    elif period.isdigit():
                        date = period
                    elif period.__contains__(':'):
                        time = period
                    else:
                        time = '00:00'

                    if len(sub_periods) == 2:
                        time = '00:00'

                    if month and date and time and start_date_time is None:
                        start_date_time = get_date_time_string(
                            year, month, date, time
                        )
                if start_date_time is not None and end_date_time is None:
                    end_date_time = get_date_time_string(
                        year, month, date, time
                    )
                    if end_date_time == start_date_time:
                        end_date_time = None
            else:
                event_info = {
                    'title': '{}: {}'.format(event_time_period, event),
                    'previous_event': get_event_title(timeline_table_rows,
                                                      tr-1),
                    'next_event': get_event_title(timeline_table_rows,
                                                  tr+1)
                }
                unsaved_events.append(event_info)
            if start_date_time:
                save_event(start_date_time, end_date_time, event)

    for event in unsaved_events:
        previous_event = CalendarEvent.objects.get(
            title=event['previous_event'])
        next_event = CalendarEvent.objects.get(title=event['next_event'])
        start_date_time = (previous_event.start_date_time
                           or previous_event.end_date_time)
        end_date_time = next_event.start_date_time
        save_event(start_date_time, end_date_time, event['title'])


@lru_cache(maxsize=32)
def save_gci_timeline():
    """
    Save the Google Code-In timeline to the database
    """
    timeline_table_rows, year = get_timeline_table_rows('gci')

    # Starting from index 1 as index 0 contains just the table headings, and
    # Ending one less than the original, as the last column contains doesn't
    # exact date-time of the event
    for tr in range(1, len(timeline_table_rows)-1):

        # Find all data in the table row
        table_cells_data = timeline_table_rows[tr].find_all('td')

        # First column of row contains either program year or the event
        # date-time. If it has a `strong` tag, then it is an year else it is
        # a date-time string
        year_table_entry = table_cells_data[0].find_all('strong')

        if len(year_table_entry) != 0:
            year = year_table_entry[0].get_text()
        else:
            event_time_period = table_cells_data[0].get_text()

            # Column 1 of the row contains the event description or say, title
            event = table_cells_data[1].get_text()

            month = None
            date = None
            time = '00:00'

            # Event time period is of the following format:
            # Day, Month Year(UTC TIME)
            event_time_period = event_time_period.split(', ')
            event_time_period = event_time_period[1]
            if event_time_period.endswith('UTC)'):
                event_time_period = event_time_period.replace('(', '')
                event_time_period = event_time_period.replace(')', '')
                event_time_period = event_time_period.replace('UTC', '')
            sub_periods = event_time_period.split()
            for period in sub_periods:
                if period.isalpha():
                    month = get_month(period)
                elif period.isdigit():
                    date = period
                else:
                    time = period

            if month and date:
                start_time = get_date_time_string(year, month, date, time)
                save_event(start_date_time=start_time, event=event)


def get_timeline_table_rows(program_name):
    """
    Scrape all the table rows from the web html content of the program
    timeline
    :param program_name: The name of the program - GSoC or GCI
    :return: A tuple containing first value as a list of all table rows
    in the timeline, and the second as the program year
    """
    timeline_url = ('https://developers.google.com/open-source/{}/timeline'
                    .format(program_name))
    timeline_html_content = requests.get(timeline_url).content
    bs4_parser = BeautifulSoup(timeline_html_content, 'html.parser')
    timeline_table_rows = bs4_parser.find_all('tr')
    year = datetime.datetime.today().year
    return timeline_table_rows, year


def get_month(time_period):
    """
    Get the abbreviated or full name of the month, based on the event time
    period
    :param time_period: The event time-period
    :return: The abbreviated or full name of the month
    """
    try:
        return datetime.datetime.strptime(time_period, '%B').month
    except ValueError:
        time_period = time_period[0:3]
        return datetime.datetime.strptime(time_period, '%b').month


def get_date_time_string(year, month, date, time):
    """
    Frame a custom date-time string
    :param year: In which year did event took place?
    :param month: In which month did event took place?
    :param date: on which day did event took place?
    :param time: At what time did event took place?
    :return: a formatted string
    """
    return '{Y}-{M}-{D} {T}'.format(Y=year, M=month, D=date, T=time)


def get_event_title(bs4_data_structure, index):
    """
    Get the event title by scrapping the web html
    :param bs4_data_structure: The BeautifulSoup Object, containing the web
    html scrapped data
    :param index: The column index, in which there is the name of the event
    :return: event name
    """
    return bs4_data_structure[index].find_all('td')[1].get_text()


def save_event(start_date_time=None, end_date_time=None, event=None):
    """
    Save the event details to the database
    :param start_date_time: When will the event start?
    :param end_date_time: When will the event end?
    :param event: The event title
    """
    event_data = {
        'user': 'KVGarg',
        'title': event,
        'start_date_time': start_date_time,
        'end_date_time': end_date_time
    }
    record = CalendarEventsSerializer(data=event_data)
    if record.is_valid():
        record.save()
