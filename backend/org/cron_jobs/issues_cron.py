import uuid
from functools import lru_cache

from org.git import get_igitt_org
from igitt_django.models import IGittIssue, IGittRepository


@lru_cache(maxsize=32)
def get_issues(hoster):
    org = get_igitt_org(hoster)
    repos = org.repositories
    for repo in repos:
        repo = save_repository(repo, hoster)
        issues = repo.filter_issues(state='all')
        for issue in issues:
            save_issue(hoster, repo, issue)


def save_repository(repo, hoster):
    repo_id = repo.identifier
    repositories = IGittRepository.objects.filter(id=repo_id)
    if repositories.exists():
        r = repositories.first()
    else:
        r = IGittRepository()
        r.id = repo_id
    r.full_name = repo.full_name
    r.hoster = hoster
    r.save()
    return r


def save_issue(hoster, repo, issue):
    issue_id, data = get_issue_data(hoster, repo, issue)
    issues = IGittIssue.objects.filter(number=data['number'], repo_id=repo.id)
    if issues.exists():
        i = issues.first()
    else:
        i = IGittIssue()
        i.id = issue_id
    i.number = data['number']
    i.repo_id = repo.id
    i.data = data
    i.save()
    return data


def get_issue_data(hoster, repo, issue):
    id = uuid.uuid4().int & (1 << 32) - 1
    number = issue.number
    url = (
            'https://' + hoster + '.com/' + repo.full_name + '/issues/' +
            str(number)
    )
    data = {
        'number': number,
        'title': issue.title,
        'description': issue.description,
        'url': url,
        'author': issue.author.username,
        'created_at': issue.created,
        'updated_at': issue.updated,
        'state': issue.state.value,
        'repo': repo.full_name,
        'repo_id': repo.id,
        'reactions': [r.name for r in issue.reactions],
        'assignees': [
            assignee.username for assignee in issue.assignees],
        'labels': [label for label in issue.labels],
        'mrs_closed_by': [int(i.number) for i in issue.mrs_closed_by],
        'hoster': hoster,
    }
    return id, data
