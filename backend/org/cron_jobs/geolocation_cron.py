import getorg
import logging
import json
from github import Github
from functools import lru_cache

from coala_web.settings import GITHUB_API_KEY, ORG_NAME
from org.models import Contributor, Team


@lru_cache(maxsize=32)
def fetch_contributors_geolocation():
    github = Github(login_or_token=GITHUB_API_KEY)
    organizations = [ORG_NAME, ]
    org_map, locations, metadata = getorg.orgmap.map_orgs(
        github, organizations, debug=0
    )
    logging.debug(
        '{} org geo-location metadata: {}'.format(ORG_NAME, metadata)
    )
    for user in locations.keys():
        user_location = locations[user]
        if user_location:
            contrib, created = Contributor.objects.get_or_create(login=user)
            if created:
                contrib = set_user_data(github, user, contrib)
            location = {'longitude': user_location.longitude,
                        'latitude': user_location.latitude}
            contrib.location = json.dumps(location)
            contrib.save()


def set_user_data(github, user, contrib):
    """
    Set the Contributor data object values
    :param github: An instance of GitHub object
    :param user: GitHub username
    :param contrib: The contributor data object
    :return: the contributor
    """
    user = github.get_user(user)
    team = Team.objects.get(name='{} newcomers'.format(ORG_NAME))
    contrib.name = user.name
    contrib.bio = user.bio
    contrib.num_commits = 0
    contrib.reviews = 0
    contrib.issues_opened = 0
    contrib.public_repos = user.public_repos
    contrib.public_gists = user.public_gists
    contrib.followers = user.followers
    contrib.teams.add(team)
    return contrib
