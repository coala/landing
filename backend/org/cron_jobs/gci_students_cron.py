import os

import logging
import ruamel.yaml
from functools import lru_cache

from org.models import Contributor
from utils.fetch_deployed_data import get_data


@lru_cache(maxsize=32)
def fetch():
    logger = logging.getLogger(__name__)

    get_data(repo_name='community', hoster='github', allow_failure=True,
             filenames=['static/instances.yaml'], output_dir=os.getcwd())

    try:
        with open(os.path.join(os.getcwd(), 'instances.yaml')) as f:
            parsed_yaml = ruamel.yaml.load(f, Loader=ruamel.yaml.Loader)
        students = []
        for instance in parsed_yaml.values():
            student_name = instance['student_display_name']
            if student_name not in students:
                students.append(student_name)
        for student in students:
            contributor_objs = Contributor.objects.filter(name=student)
            if contributor_objs.exists():
                contributor = contributor_objs.first()
                contributor.is_gci_participant = True
                contributor.save()
            else:
                logger.error(
                    "Contributor named '{name}' doesn't exist".format(
                        name=student
                    )
                )

    except Exception as ex:
        logger.error('load instances.yaml error: %s' % ex)
