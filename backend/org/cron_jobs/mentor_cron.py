import os
import json

from functools import lru_cache
import logging

from github import Github

from coala_web.settings import NETLIFY, ORG_NAME, GITHUB_API_KEY
from org.serializers import MentorSerializer
from org.models import Contributor, Mentor
from org.git import get_org
from utilities import send_email


ORG = get_org()
github_obj = Github(GITHUB_API_KEY)


@lru_cache(maxsize=32)
def fetch_submissions():
    form_name = os.environ.get('MENTOR_FORM_NAME')

    if not form_name:
        logging.error('Mentor form name not provided! Please set environment'
                      ' variable MENTOR_FORM_NAME.')
        exit()
    else:
        # Get all contributors request who want to participate as a mentor
        submissions = NETLIFY.get_submissions(form_name)

        for entry in submissions:
            mentor_ = entry['data']
            username = mentor_['user']
            year = mentor_['year']
            program = mentor_['program']
            # Get MentorSerializer instance using data
            mentor, contrib_exists = get_serializer_instance(
                username, year, program
            )

            if contrib_exists:
                if mentor.is_valid():
                    mentor.save()
                else:
                    logging.error('Submission {form_id} not saved for form'
                                  '{form_name}.'.format(form_id=entry['id'],
                                                        form_name=form_name))
                    # Send an email to user if any validation errors take
                    # place
                    prepare_and_send_to_user(mentor.errors, username)

            NETLIFY.delete_submission(entry['id'])


def get_serializer_instance(username, year, program):
    contributors = Contributor.objects.filter(login=username)

    if contributors:
        contrib = contributors.first()
        mentors = Mentor.objects.filter(user=contrib)
        if mentors:
            # Update data of mentor, who had mentored in previous years(s) too
            mentor = mentors.first()
            stats = json.loads(mentor.mentoring_stats)
            if program in stats.keys():
                if year not in stats[program]:
                    stats[program].append(year)
            else:
                stats[program] = [year, ]
            data = {
                'user': mentor.user.login,
                'mentoring_stats': json.dumps(stats)
            }
            return MentorSerializer(mentor, data=data), True
        else:
            # Add a new mentor to the database
            data = {
                'user': username,
                'mentoring_stats': json.dumps({
                    program: [year, ]
                })
            }
            return MentorSerializer(data=data), True

    return None, False


def get_email(user):
    """
    Get the user email
    :param user: the github userna,e
    :return: user email
    """
    user_obj = github_obj.get_user(user)
    return user_obj.email


def prepare_and_send_to_user(errors, username):
    """
    Send an email to user by preparing the email message based on the
    validation errors
    :param errors: A dict of errors
    :param username: The github username
    """
    user_email = get_email(username)
    subject = '{}: Mentor form rejected!'.format(ORG_NAME)
    message_body = (
        'Hi {username}!\nThere were some errors in your Mentor form'
        ' submission.\nFollowing were the errors:\n{errors_info}\nPlease'
        ' correct the errors and re-fill the form.\nNote: Make sure to read'
        ' the hint(s) or placeholder(s), if any, while filling up the form.'
    )
    field_errors = {}
    for error_field, error in errors.items():
        error_log = '\n'.join([err for err in error])
        field_errors[error_field] = error_log
    errors_info = '\n'.join(
        [
            '{field_name}: {err}'.format(field_name=k, err=v)
            for k, v in field_errors.items()
        ]
    )
    message = message_body.format(username=username,
                                  errors_info=errors_info)
    send_email(subject, user_email, message)
