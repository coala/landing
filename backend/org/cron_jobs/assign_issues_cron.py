import os
import re
import json

from functools import lru_cache
import logging

from github import Github
from IGitt.GitHub.GitHubRepository import GitHubRepository, GitHubToken
from IGitt.GitLab.GitLab import GitLabPrivateToken
from IGitt.GitLab.GitLabRepository import GitLabRepository

from coala_web.settings import (
    NETLIFY,
    ORG_NAME,
    GITHUB_API_KEY,
    GITLAB_API_KEY
)
from org.git import get_org
from org.models import Contributor, AssignIssueRequest
from utilities import send_email

ORG = get_org()
GITHUB = Github(GITHUB_API_KEY)
GH_BASE_URL = 'https://github.com/'
GL_BASE_URL = 'https://gitlab.com/'


@lru_cache(maxsize=32)
def fetch_submissions():
    form_name = os.environ.get('ISSUES_ASSIGN_REQUEST_FORM_NAME')

    # Check whether the environment variable is set or not!
    if not form_name:
        logging.error('The issues assign request form name is not provided!'
                      'Please set environment variable'
                      ' ISSUES_ASSIGN_REQUEST_FORM_NAME.')
        exit()
    else:
        submissions = NETLIFY.get_submissions(form_name)

        # Validate each user submission
        for entry in submissions:
            issue_ = entry['data']
            issue_data = get_assigning_issue_data(issue_)
            valid_issue, errors = check_user_submission(issue_data)

            # If valid, save the `getting assigned on an issue` request and
            # send an email to user, to open the link which will assign the
            # issue to the user.
            if valid_issue:
                obj = save_assign_issue_request(issue_data)
                prepare_and_send_to_user(None, issue_data, obj)
            else:
                logging.error('Submission {form_id} not saved for form'
                              '{form_name}.'.format(form_id=entry['id'],
                                                    form_name=form_name))
                prepare_and_send_to_user(errors, issue_data)

            NETLIFY.delete_submission(entry['id'])


def get_assigning_issue_data(issue_):
    """
    Get the submitted data by the contributor
    :param issue_: The form data entry
    :return: A dict containing the issue related details
    """
    hoster, repository, number = get_issue_details(issue_['url'])
    return {
        'user': issue_['user'],
        'requested_user': issue_['requested_by_user'],
        'hoster': hoster,
        'repository': repository,
        'number': number
    }


def get_issue_details(web_url):
    """
    Extract the Hoster, Repository and the Issue number from the issue url
    :param web_url: The issue web url
    :return: a tuple of extracted data
    """
    match = re.match(
        r'https://(github|gitlab)\.com/(.+[^/]+)/issues/(\d+)', web_url
    )
    if not match:
        return None, None, None
    return (
        match.group(1),
        match.group(2),
        int(match.group(3))
    )


def check_user_submission(data):
    """
    Check whether the submitted data by the user is valid or not!
    :param data: Th issue related data, submitted by user
    :return: A Boolean
    """
    username = data['user']
    if username == data['requested_user']:
        contributors = Contributor.objects.filter(login=username)
        if contributors:
            contrib = contributors.first()
            working_on_issues_count = json.loads(
                contrib.working_on_issues_count
            )
            if working_on_issues_count[data['hoster']] < 2:
                return validate_repository_and_issue(data)
            else:
                errors = ("You're already working on 2 or more issues. Please"
                          ' solve those issues first before getting assigned'
                          ' onto a new issue.')
        else:
            errors = ('Sorry! But you are not a member of organization as we'
                      ' cannot find you in our database. Please try again'
                      ' later, after some hours.')
    else:
        errors = ("You can't assign an issue to some other contributor! Please"
                  'take care from next time, else you will be blacklisted.')
    return False, errors


def validate_repository_and_issue(data):
    """
    Validate whether the repository and issue exists or not!
    :param data: the issue related details
    :return: A Boolean with a list of errors
    """
    issue_number = data['number']
    repository = get_igitt_repository_object(data)
    if repository is not None:
        issue = repository.get_issue(issue_number)
        return validate_issue(repository, issue)
    else:
        errors = ('Not a valid repository {url} ! Please provide a correct'
                  ' repository url'.format(url=data['repository_url']))
    return False, errors


def get_igitt_repository_object(data):
    """
    Get an instance of IGitt GitHub/GitLab repository based on submitted data
    by the user
    :param data: The submitted data by the user
    :return: an instance of igitt repository
    """
    hoster = data['hoster']
    repository_full_name = data['repository']
    if hoster == 'github':
        repository = GitHubRepository(GitHubToken(GITHUB_API_KEY),
                                      repository_full_name)
    else:
        repository = GitLabRepository(GitLabPrivateToken(GITLAB_API_KEY),
                                      repository_full_name)
    try:
        if repository.parent is None:
            return repository
        else:
            return None
    except RuntimeError:
        return None


def validate_issue(repository, issue):
    """
    Validate whether the issue is valid or not! And, is it already being
    assigned to any other contrib or not?
    :param repository: an instance of igitt repository
    :param issue: an instance of igitt issue
    :return: a boolean with a list of errors
    """
    users = list()
    try:
        assignees = issue.assignees
        if len(assignees) == 0:
            return True, None
        for assignee in assignees:
            users.append(assignee.username)
        users_str = ', '.join(users)
        errors = (
            'The issue is already being assigned to user(s) {users}! Cannot'
            ' assign to more users.'.format(users=users_str)
        )
    except RuntimeError:
        errors = (
            'Invalid issue number provided. The repository {repository} has'
            ' no issue number {number}'.format(repository=repository.full_name,
                                               number=issue.number)
        )
    return False, errors


def save_assign_issue_request(data):
    """
    Save the request to the database
    :param data: The submitted data by the user
    :return: instance of saved request
    """
    return AssignIssueRequest.objects.create(
        username=data['user'],
        hoster=data['hoster'],
        repository_name=data['repository'],
        issue_number=data['number']
    )


def get_email(user):
    """
    Get public visible github user email
    :param user: github username
    :return: user email
    """
    user_obj = GITHUB.get_user(user)
    return user_obj.email


def prepare_and_send_to_user(errors, data, assign_request_obj=None):
    """
    Send an email to the user. If no errors, send a link which can assign the
    issue to the user. If errors, send an email reporting the validation
    errors
    :param errors: A dict of errors
    :param data: the issue data submitted by user
    :param assign_request_obj: an instance of AssignIssueRequest model
    """
    user_email = get_email(data['user'])
    message = (
        'You created a request to get assigned on a {hoster} issue, numbered'
        ' {number} in repository {url}. '.format(
            hoster=data['hoster'], number=str(data['number']),
            url=data['repository_url']
        ))
    if errors is None:
        subject = '{org}: Request to get assigned on the issue APPROVED!'
        message += (
             'Please click the below link to get it assigned.\n{link}'.format(
                link=get_issue_assigning_link(assign_request_obj)
             )
        )
    else:
        subject = '{org}: Request to get assigned on the issue REJECTED!'
        message += (
            'But there was an error while we were validating your request!'
            '\nError Log - {error}'.format(
                error=errors
            )
        )
    subject = subject.format(org=ORG_NAME)
    send_email(subject, user_email, message)


def get_issue_assigning_link(assign_request_obj):
    """
    Create a issue assigning link for the user, upon when opened in the
     browser it will assign the issue to the user
    :param assign_request_obj: An instance of model
    :return: the assigning link
    """
    query_param = 'hoster={}&repo={}&issue={}&user={}'.format(
        assign_request_obj.hoster,
        assign_request_obj.repository_name,
        assign_request_obj.issue_number,
        assign_request_obj.username
    )
    return 'https://webservices.{org}.io/assign/?{params}'.format(
        org=ORG_NAME, params=query_param
    )
