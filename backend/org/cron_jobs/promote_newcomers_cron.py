import os

import yaml
import requests
from datetime import datetime

import logging
from functools import lru_cache

from IGitt.GitHub import GitHub
from IGitt.GitHub.GitHubRepository import GitHubRepository, GitHubIssue
from IGitt.GitHub.GitHubMergeRequest import GitHubMergeRequest
from IGitt.GitLab.GitLabMergeRequest import GitLabMergeRequest
from IGitt.GitLab.GitLabRepository import GitLabRepository, GitLabIssue
from IGitt.GitHub.GitHub import GitHub as GitHubMain
from IGitt.GitLab.GitLab import GitLab as GitLabMain
from IGitt.Interfaces import get_response
from IGitt.GitHub.GitHubUser import GitHubUser
from IGitt.GitLab.GitLabUser import GitLabUser, GitLabPrivateToken

from coala_web.settings import ORG_NAME, NETLIFY, GITLAB_API_KEY
from org.git import get_igitt_org, GITHUB_TOKEN
from org.models import Contributor, BlacklistedContributorStatus
from utils.check_for_valid_bears import check_for_valid_bears
from utilities import send_email

GITHUB_ORG = get_igitt_org('github')
GITLAB_ORG = get_igitt_org('gitlab')
GITLAB_TOKEN = GitLabPrivateToken(GITLAB_API_KEY)
GH_BASE_URL = 'https://github.com/'
GL_BASE_URL = 'https://gitlab.com/'
ORG_API_DOCUMENTATION = 'https://api.{org}.io/'.format(org=ORG_NAME)
ORG_DOCUMENTATION = 'https://docs.{org}.io/'.format(org=ORG_NAME)


class PromoteNewcomer:

    def __init__(self, entry, form_id, form_name):
        """
        :param entry_data: Data submitted by user when requested to join org
        :param form_id: Unique ID of NEWCOMER_PROMOTION_REQUEST_FORM_NAME
        :param form_name: NEWCOMER_PROMOTION_REQUEST_FORM_NAME
        """
        self.form_id = form_id
        self.form_name = form_name
        self.user_data = self.get_submitted_data(entry)
        self.github_user_data = None
        self.gitlab_user_data = None
        self.username = None
        self.user_id = None
        self.coafile_sections = None
        self.coafile_bears = None
        self.errors = list()
        self.session = requests.Session()

    def get_submitted_data(self, entry):
        """
        :param data: Submitted data by user
        :return: A dict of information to be used in-program
        """
        data = entry['data']
        return {
            'instance': {
                'github': GitHubUser(GITHUB_TOKEN, data['github_username']),
                'gitlab': GitLabUser(GITLAB_TOKEN, data['gitlab_user_id'])
            },
            'project_details': {
                'web_url': data['project_web_url'],
                'build_file_relative_path': data['build_file_relative_path'],
                'coafile_relative_path': data['coafile_relative_path'],
                'gist_or_snippet_id': data['gist_or_snippet_id']
            },
            'newcomer_issue': {
                'solved_web_url': data['newcomer_solved_issue_web_url'],
                'related_pr': data['newcomer_issue_related_pr'],
                'pr_reviewed_url': data['newcomer_issue_pr_reviewed_url']
            },
            'low_level_issue': {
                'solved_web_url': data['low_level_solved_issue_web_url'],
                'related_pr': data['low_level_issue_related_pr'],
                'pr_reviewed_url': data['low_level_issue_pr_reviewed_url']
            },
            'requested_user': data['request_created_by_user'],
            'submitted_on': datetime.strptime(entry['created_at'],
                                              '%Y-%m-%dT%H:%M:%S.%fZ'),
        }

    def validate_submitted_data(self):
        """
        Check whether the request created by user isn't a false request
        by checking the usename against the predefined value. Afterwards,
        check whether the contributor already exists or not, and isn't
        blacklisted already due to too many false requests created. If
        all above are valid and is not blacklisted, validate whether the
        user is eligible to become a developer or not
        :return: Whether the hoster accounts are valid or not, the contrib
         exists or not,and the contributor is already a developer or not
        """
        valid_user = self.validate_hoster_user_details()
        contributors = Contributor.objects.filter(login=self.username)
        blacklisted_contribs = BlacklistedContributorStatus.objects.filter(
            username=self.username, developer_request_count_gt=3
        )
        is_a_developer = False
        if contributors.exists() and not blacklisted_contribs.exists():
            contrib = contributors.first()
            developers_team = '{org} developers'.format(org=ORG_NAME)
            developer_team_member = contrib.teams.filter(name=developers_team)
            if not developer_team_member.exists():
                if valid_user:
                    self.validate_newcomer()
            else:
                is_a_developer = True
                self.errors.append("You're already a member of developers"
                                   ' team on GitHub!')
        elif blacklisted_contribs.exists():
            self.errors.append(
                "You're blacklisted from applying this request! You have"
                ' already applied 3 times for promotion to developer role!'
            )
        else:
            self.errors.append("You're not a member of the organization!")
        return valid_user, contributors.exists(), is_a_developer

    def validate_hoster_user_details(self):
        """
        Check whether the the GitHub username and GitLab user ID
        is correct or not. If not correct a RuntimeError will be
        raised and the corresponding possible error will be added
        to error list. If no error raised, the user Github & GitLab
        details will be stored as instance variables.
        :return: Whether the provided user details are correct or not.
        """
        gh_instance = self.user_data['instance']['github']
        gl_instance = self.user_data['instance']['github']
        try:
            self.github_user_data = gh_instance.data.get()
            self.username = self.github_user_data['login']
            if self.username != self.user_data['requested_user']:
                raise RuntimeError
        except RuntimeError:
            self.errors.append('Your GitHub username is Incorrect! Please'
                               " don't alter the predefined values!")
        try:
            gl_instance.data.refresh()
            self.gitlab_user_data = gl_instance.data.get()
            self.user_id = self.gitlab_user_data['id']
        except RuntimeError:
            self.errors.append('Your GitLab user-id is Incorrect! Please'
                               ' provide correct user id.')
        if self.errors:
            return False
        return True

    def validate_newcomer(self):
        """
        Perform the validation checks to validate whether the newcomer
        is eligible to become a developer or not. If all the checks
        are passed, the newcomer will be invited to join developers
        group and added to GitLab developers group. If checks failed,
        the user will added into BlackListed contrib with an initial
        value to 1 which will be incremented whenever the false request
        is being created by user.
        """
        valid_newcomer = self.perform_validation_checks()
        if valid_newcomer:
            self.add_newcomer_to_developers_team()
        else:
            contrib, c = BlacklistedContributorStatus.objects.get_or_create(
                username=self.username
            )
            contrib.developer_request_count += 1
            contrib.save()
        if self.errors:
            logging.error('Submission {form_id} not saved for form'
                          ' {form_name}.'.format(form_id=self.form_id,
                                                 form_name=self.form_name))

    def perform_validation_checks(self):
        """
        To validate whether a newcomer is eligible to become a developer or
        not, we need to check whether the user has used linter in one of his/
        her personal projects, has contributed to newcomer issues - solved &
        reviewed, and has contributed to low difficulty level issues - solved
        & reviewed. And get all the possible errors for the failed checks
        :return: Whether the newcomer is eligible to be developer or not
        """
        linter_usage_in_project = self.check_linter_usage_in_project()
        newcomer_contributions = self.has_contributed_to_newcomer_issues()
        low_level_contributions = self.has_contributed_to_low_level_issues()
        newcomer_eligible = all([
            linter_usage_in_project, newcomer_contributions,
            low_level_contributions
        ])
        self.get_error_messages(
            linter_usage_in_project, newcomer_contributions,
            low_level_contributions
        )
        return newcomer_eligible

    def check_linter_usage_in_project(self):
        """
        Check whether the user has used linter in the provided project
        URL. To validate it, we need check whether the build is passing
        or not. If it is, validate the build file to check whether it
        contains the run command or not; validate the coafile to find
        the sections and valid bears; and the terminal output that it
        contains no severity errors.
        :return: A boolean, whether there is linter usage in project or not
        """
        hoster, repository, build_status = self.get_project_build_status()
        if build_status == 'success':
            build_valid = self.validate_ci_build_file(hoster, repository)
            coafile_valid = self.validate_coafile(hoster, repository)
            terminal_output_valid = self.validate_terminal_output(hoster)
            return all([build_valid, coafile_valid, terminal_output_valid])
        return False

    def get_project_build_status(self):
        """
        Get the project build status whether it is green or not!
        :return: The hoster, repository name and the build state
        """
        project_url = self.user_data['project_details']['web_url']
        hoster, repo_name = self.get_repository_and_hoster(project_url)
        if hoster == 'github':
            repo = GitHubRepository(GITHUB_TOKEN, repo_name)
            try:
                repo_status_url = ('{repo_api_url}/commits/master/status'
                                   .format(repo_api_url=repo.url))
                r_json, other_data = get_response(
                    self.session.get, repo_status_url, GITHUB_TOKEN.auth
                )
                ci_state = r_json['state']
            except RuntimeError:
                ci_state = 'failure'
        else:
            repo = GitLabRepository(GITLAB_TOKEN, repo_name)
            try:
                repo_status_url = ('{repo_api_url}/repository/commits/master'
                                   .format(repo_api_url=repo.url))
                r_json, other_data = get_response(
                    self.session.get, repo_status_url, GITLAB_TOKEN.auth
                )
                ci_state = r_json['status']
            except RuntimeError:
                ci_state = 'failure'
        return hoster, repo, ci_state

    def validate_ci_build_file(self, hoster, repository):
        """
        Validate whether the build file contains the linter run
        command or not, under the script section!
        :param hoster: Either of two values - github or gitlab
        :param repository: Hoster repository instance
        :return: whether the build file is valid or not
        """
        project_details = self.user_data['project_details']
        build_file = project_details['build_file_relative_path']
        if not build_file.endswith('.yml'):
            return False
        if hoster == 'github':
            file_content = self.download_github_file_content(repository,
                                                             build_file)
        else:
            file_content = self.download_gitlab_file_content(repository,
                                                             build_file)
        if file_content is not None:
            return self.check_word_in_yaml_content(file_content, ORG_NAME)
        return False

    def validate_coafile(self, hoster, repository):
        """
        Validate whether the coafile contains the linter configuration
        or not, by checking the number of sections and valid bears. If
        there are more than 5 sections and bears than the coafile is
        said to be valid
        :param hoster: Either of two values - github or gitlab
        :param repository: Hoster repository instance
        :return: whether the coafile is valid or not
        """
        project_details = self.user_data['project_details']
        coafile = project_details['coafile_relative_path']
        if not coafile.endswith('coafile'):
            return False
        if hoster == 'github':
            file_content = self.download_github_file_content(repository,
                                                             coafile)
        else:
            file_content = self.download_gitlab_file_content(repository,
                                                             coafile)
        sections = list()
        bears = list()
        if file_content is not None:
            for line in file_content.splitlines():
                stripped_line = line.strip()
                if(stripped_line.startswith('[') and
                        stripped_line.endswith(']')):
                    sections.append(stripped_line.replace('[', '')
                                    .replace(']', ''))
                elif stripped_line.__contains__('bears'):
                    bears_line = stripped_line.replace('bears', '')
                    bears_line = bears_line.replace('=', '')
                    bears_line = bears_line.strip()
                    bears.extend(bears_line.split(','))
        self.coafile_sections = set(sections)
        self.coafile_bears = set(bears)
        if len(self.coafile_sections) > 4 and len(self.coafile_bears) > 4:
            return check_for_valid_bears(bears)
        return False

    def validate_terminal_output(self, hoster):
        """
        Validate whether the terminal output of run command is valid or not!
        Can be validated by checking if there are any severity errors and
        corrections suggested by linter, and there are same number of sections
        as specified in the coafile
        :param hoster: Either of two values - github or gitlab
        :return: whether the terminal output is valid or not
        """
        project_details = self.user_data['project_details']
        file_id = project_details['gist_or_snippet_id']
        if hoster == 'github':
            file_content = self.download_gist_file_content(file_id)
        else:
            file_content = self.download_snippet_file_content(file_id)
        if file_content is not None:
            section_count = 0
            for line in file_content.splitlines():
                stripped_line = line.strip()
                if(stripped_line.lower().__contains__('error') or
                        stripped_line.lower().__contains__('severity') or
                        stripped_line.__contains__('----')):
                    return False
                elif stripped_line.__contains__('Executing section'):
                    section_count += 1
            if section_count == len(self.coafile_sections):
                return True
        return False

    def has_contributed_to_newcomer_issues(self):
        """
        Whether the newcomer has contributed to organization or not! Can
        be validated by checking if solved a newcomer difficulty issue
        and has reviewed a pull request of any newcomer
        :return: An AND of both the checks
        """
        issue_solved = self.has_solved_newcomer_issue()
        pull_request_reviewed = self.has_reviewed_newcomer_pull_request()
        return all([issue_solved, pull_request_reviewed])

    def has_solved_newcomer_issue(self):
        """
        Whether the newcomer has solved the newcomer difficult issue
        without so much assistance of other contributors. The function
        checks whether the newcomer_issue details are valid or not, and
        if valid it checks whether the contrib received too much assistance
        of other contributors or not.
        :return: A boolean, whether solved the issue or not
        """
        newcomer_issue_details = self.user_data['newcomer_issue']
        issue_url = newcomer_issue_details['solved_web_url']
        related_pr = newcomer_issue_details['related_pr']
        hoster, repo_name = self.get_repository_and_hoster(issue_url)
        issue_number = self.get_number(issue_url)
        pr_number = self.get_number(related_pr)
        try:
            if hoster == 'github':
                repository = GitHubRepository(GITHUB_TOKEN, repo_name)
            else:
                repository = GitLabRepository(GITLAB_TOKEN, repo_name)
            issue = repository.get_issue(issue_number)
            pull_request = repository.get_mr(pr_number)
        except RuntimeError:
            return False

        is_valid_newcomer_issue = self.verify_issue_details(
            issue, 'closed', 'newcomer'
        )
        is_valid_newcomer_pr = self.verify_pull_request_details(
            issue_number, pull_request, 'merged'
        )
        if is_valid_newcomer_issue and is_valid_newcomer_pr:
            return self.has_solved_without_much_assistance(
                issue=issue, pull_request=pull_request
            )
        return False

    def has_reviewed_newcomer_pull_request(self):
        """
        Whether the newcomer has reviewed the newcomer difficult issue
        pull request or not. The function gets all the reviews or discussions
        on the merge request and check if any of the discussion is being
        started by the newcomer or not.
        :return: A boolean, whether any of the discussion is being started by
        contrib or not.
        """
        newcomer_issue_details = self.user_data['newcomer_issue']
        reviewed_url = newcomer_issue_details['pr_reviewed_url']
        hoster, repo_name = self.get_repository_and_hoster(reviewed_url)
        pr_number = self.get_number(reviewed_url)
        try:
            if hoster == 'github':
                repository = GitHubRepository(GITHUB_TOKEN, repo_name)
                pull_request = repository.get_mr(pr_number)
                pr_reviews = pull_request.url + '/reviews'
                reviews = self.get_all_reviews(
                    hoster, pr_reviews, GITHUB_TOKEN.auth
                )
                for review in reviews:
                    if (review['state'] != 'APPROVED' and
                            review['user']['login'] == self.username):
                        return True
            else:
                repository = GitLabRepository(GITLAB_TOKEN, repo_name)
                pull_request = repository.get_mr(pr_number)
                pr_reviews = pull_request.url + '/discussions'
                reviews = self.get_all_reviews(
                    hoster, pr_reviews, GITLAB_TOKEN.auth
                )
                for review in reviews:
                    for note in review['notes']:
                        if (note['type'] == 'DiffNote' and
                                note['author']['id'] == self.user_id):
                            return True
        except RuntimeError:
            return False
        return False

    def has_contributed_to_low_level_issues(self):
        """
        Whether the newcomer has contributed to organization or not by
        contributing to low difficulty level issues and merge requests.
        Can be validated by checking if solved a low difficulty issue
        and has reviewed a pull request that solves a low difficulty issue.
        :return: An AND of both the checks
        """
        issue_solved = self.has_solved_low_difficulty_issue()
        pull_request_reviewed = self.has_reviewed_low_difficult_pull_request()
        return all([issue_solved, pull_request_reviewed])

    def has_solved_low_difficulty_issue(self):
        """
        Whether the newcomer has solved the low difficulty level issue
        without being directed to the org documentation. The function
        checks whether the low difficulty level issue details are valid
        or not, and if valid it checks whether the contrib has been directed
        to docs or not
        :return: A boolean, whether solved the issue or not
        """
        issue_details = self.user_data['low_level_issue']
        issue_url = issue_details['solved_web_url']
        related_pr = issue_details['related_pr']
        hoster, repo_name = self.get_repository_and_hoster(issue_url)
        issue_number = self.get_number(issue_url)
        pr_number = self.get_number(related_pr)
        try:
            if hoster == 'github':
                repository = GitHubRepository(GITHUB_TOKEN, repo_name)
            else:
                repository = GitLabRepository(GITLAB_TOKEN, repo_name)
            issue = repository.get_issue(issue_number)
            pull_request = repository.get_mr(pr_number)
        except RuntimeError:
            return False

        is_valid_low_level_issue = self.verify_issue_details(
            issue, 'closed', 'low'
        )
        if is_valid_low_level_issue:
            self_solved_issue = self.has_solved_without_docs(pull_request)
            is_valid_newcomer_pr = self.verify_pull_request_details(
                issue_number, pull_request, 'merged'
            )
            return all([self_solved_issue, is_valid_newcomer_pr])
        return False

    def has_reviewed_low_difficult_pull_request(self):
        """
        Whether the newcomer has reviewed a low difficulty level issue
        pull request or not. The function gets all the reviews or discussions
        on the merge request and check if any of the discussion is being
        ended by the newcomer or not.
        :return: A boolean, whether any of the discussion is being ended by
        contrib or not.
        """
        newcomer_issue_details = self.user_data['low_level_issue']
        reviewed_url = newcomer_issue_details['pr_reviewed_url']
        hoster, repo_name = self.get_repository_and_hoster(reviewed_url)
        pr_number = self.get_number(reviewed_url)
        try:
            if hoster == 'github':
                repository = GitHubRepository(GITHUB_TOKEN, repo_name)
                pull_request = repository.get_mr(pr_number)
                pr_reviews = pull_request.url + '/reviews'
                reviews = self.get_all_reviews(
                    hoster, pr_reviews, GITHUB_TOKEN.auth, reverse=True
                )
                for review in reviews:
                    if review['state'] != 'APPROVED':
                        if review['user']['login'] == self.username:
                            return True
                        else:
                            return False
            else:
                repository = GitLabRepository(GITLAB_TOKEN, repo_name)
                pull_request = repository.get_mr(pr_number)
                pr_reviews = pull_request.url + '/discussions'
                reviews = self.get_all_reviews(
                    hoster, pr_reviews, GITLAB_TOKEN.auth, reverse=True
                )
                for review in reviews:
                    for note in review['notes']:
                        if note['type'] == 'DiffNote':
                            if note['author']['id'] == self.user_id:
                                return True
                            else:
                                return False
        except RuntimeError:
            return False

    def get_repository_and_hoster(self, web_url: str):
        """
        :param web_url: The issue or pull request web url
        :return: the extracted repository name and the hoster
        from the web_url
        """
        if web_url.__contains__('github'):
            repository_name = web_url.replace(GH_BASE_URL, '')
            hoster = 'github'
        else:
            repository_name = web_url.replace(GL_BASE_URL, '')
            hoster = 'gitlab'
        if repository_name.endswith('/'):
            repository_name = repository_name[:-1]
        return hoster, repository_name

    def get_number(self, web_url):
        """
        :param web_url: The issue or pull request web url
        :return: the number of issue or pull request
        """
        if web_url.endswith('/'):
            web_url = web_url[:-1]
        return int(web_url.split('/')[-1])

    def download_github_file_content(self, repository, file_path):
        """
        Download the GitHub repository file content
        :param repository: an instance of Hoster Repository
        :param file_path: The relative file path in the project directory
        :return: The file content if valid details provided, otherwise None
        """
        file_api_url = (
            '{repo_api_url}/contents/{file_path}'
            .format(repo_api_url=repository.url, file_path=file_path)
        )
        try:
            r_json, other_data = get_response(
                self.session.get, file_api_url, GITHUB_TOKEN.auth
            )
        except RuntimeError:
            return None
        if isinstance(r_json, dict):
            content_download_url = r_json.get('download_url', None)
            if content_download_url is not None:
                file_content, other_data = get_response(
                    self.session.get, content_download_url, GITHUB_TOKEN.auth
                )
                return file_content
        return None

    def download_gitlab_file_content(self, repository, file_path):
        """
        Download the GitLab project file content
        :param repository: an instance of Hoster Repository
        :param file_path: The relative file path in the project directory
        :return: The file content if valid details provided, otherwise None
        """
        file_api_url = (
            '{repo_api_url}/repository/files/{file_path}/raw?ref=master'
            .format(repo_api_url=repository.url, file_path=file_path)
        )
        try:
            file_content, other_data = get_response(
                self.session.get, file_api_url, GITLAB_TOKEN.auth
            )
            return file_content
        except RuntimeError:
            return None

    def download_gist_file_content(self, file_id):
        """
        Download the GitHub Gist file content having file_id
        :param file_id: The Gist file id
        :return: The file content if valid id provided, otherwise None
        """
        file_api_url = GitHubMain.absolute_url(
            '/gists/{id}'.format(id=file_id)
        )
        try:
            r_json, other_data = get_response(
                self.session.get, file_api_url, GITHUB_TOKEN.auth
            )
        except RuntimeError:
            return None
        if isinstance(r_json, dict):
            files = r_json['files']
            for name, file_info in files.items():
                if name.endswith('.sh'):
                    file_raw_url = file_info['raw_url']
                    file_content, other_data = get_response(
                        self.session.get, file_raw_url, GITHUB_TOKEN.auth
                    )
                    return file_content
        return None

    def download_snippet_file_content(self, file_id):
        """
        Download the GitLab snippet file content having file_id
        :param file_id: The snippet file id
        :return: The file content if valid id provided, otherwise None
        """
        file_api_url = GitLabMain.absolute_url(
            '/snippets/{id}'.format(id=file_id)
        )
        try:
            r_json, other_data = get_response(
                self.session.get, file_api_url, GITLAB_TOKEN.auth
            )
        except RuntimeError:
            return None
        if r_json['file_name'].endswith('.sh'):
            file_raw_url = r_json['raw_url']
            file_content, other_data = get_response(
                self.session.get, file_raw_url, GITLAB_TOKEN.auth
            )
            return file_content
        return None

    def check_word_in_yaml_content(self, yml_content, word):
        """
        Check whether the YAML file content contains a given word or not
        :param yml_content: The yaml file content
        :param word: word to be checked in the file
        :return: A boolean value, whether the file consists of this word
        or not
        """
        with open('check_file.yml', 'w+') as f:
            f.write(yml_content)
        with open('check_file.yml') as f:
            data = yaml.safe_load(f)
            if 'script' in data.keys():
                for cmd in data['script']:
                    if not cmd.__contains__('#') and cmd.__contains__(word):
                        return True
        return False

    def verify_issue_details(self, issue, state, label_name):
        """
        Verify whether the issue is in provided state or not, is being
        assigned to the requested user, and has a valid provided label.
        :param issue: An instance of Hoster Issue
        :param state: The state of issue in which it might be
        :param label_name: The issue labelled with a particular name
        :return: A boolean value
        """
        valid_state = issue.state == state
        assigned_to_requested_user = False
        has_valid_label = False

        assignees = issue.assignees
        for assignee in assignees:
            if assignee.username == self.username:
                assigned_to_requested_user = True
                break

        labels = issue.labels
        for label in labels:
            if label.__contains__(label_name):
                has_valid_label = True
                break
        return all([valid_state, assigned_to_requested_user, has_valid_label])

    def verify_pull_request_details(self, issue_number, pull_request,
                                    pr_state):
        """
        Verify whether the pull request is in provided state or not, and
        closes the provided issue or not
        :param issue_number: The issue number which is being closed by pull
         request
        :param pull_request: An instance of Hoster MergeRequest
        :param pr_state: The state in which the pr must be
        :return: A boolean value
        """
        valid_state = pull_request.state == pr_state
        closes_passed_issue = False
        for issue in pull_request.closes_issues:
            if issue.number == issue_number:
                closes_passed_issue = True
                break
        return valid_state and closes_passed_issue

    def has_solved_without_much_assistance(self, issue, pull_request):
        """
        Whether the newcomer has solved the issue without much assistance
        of other contribs! Count every reply or doubt by the newcomer. Each
        of it counts as an ask for assistance.
        :param issue: An instance of Hoster Issue
        :param pull_request: An instance of Hoster MergeRequest
        :return: A boolean value
        """
        on_issue_comment_count = self.get_comment_count_by_user(issue)
        on_pr_comment_count = self.get_comment_count_by_user(pull_request)
        if on_issue_comment_count + on_pr_comment_count <= 10:
            return True
        return False

    def has_solved_without_docs(
            self, pull_request: [GitHubMergeRequest, GitLabMergeRequest]
    ):
        """
        Whether the user has solved the issue without being directed to
        the organization documentation or not
        :param pull_request: An instance of Hoster MergeRequest
        :return: A boolean value
        """
        comments = pull_request.comments
        for comment in comments:
            if comment.author.username != self.username:
                body = comment.body
                if (body.__contains__(ORG_API_DOCUMENTATION) or
                        body.__contains__(ORG_DOCUMENTATION)):
                    return False
        return True

    def get_comment_count_by_user(
            self, issue: [GitHubIssue, GitLabIssue, GitHubMergeRequest,
                          GitLabMergeRequest]
    ):
        """
        Find the number of comments made by user on the issue or pull
        request
        :param issue: AN instance of Hoster Issue or MergeRequest
        :return: number of comments
        """
        on_issue_comments = issue.comments
        comments_count = 0
        for comment in on_issue_comments:
            if comment.author.username == self.username:
                comments_count += 1
        return comments_count

    def get_all_reviews(self, hoster, web_url, auth, reverse=False):
        """
        Get all the reviews/discussions of a merge_request
        :param hoster: Either of two values - github or gitlab
        :param web_url: The web api url of merge request
        :param auth: An instance of AuthBase
        :param reverse: Whether to return the results in ascending
        order or descending order.
        :return: A list of all reviews
        """
        next_url = True
        results = list()
        while next_url:
            r_json, other_data = get_response(self.session.get, web_url, auth)
            if hoster == 'github':
                results.extend(r_json.get('items'))
            else:
                results.extend(r_json)
            if 'next' in other_data.keys():
                web_url = other_data.get('next').get('url')
            else:
                next_url = False
        if reverse:
            return results.reverse()
        return results

    def add_newcomer_to_developers_team(self):
        """
        After newcomer is eligible to become a developer, add newcomer to the
        respective developers team
        """
        try:
            self.add_to_gitlab_developers_group()
            self.invite_to_developers_team()
        except RuntimeError:
            self.errors.append(
                'There was an error while we were trying to add you to the'
                ' developers team. Please contact the contact the maintainers'
                ' mentioning about this issue.'
            )

    def add_to_gitlab_developers_group(self):
        """
        Add the newcomer to the GitLab developers team of the organization
        """
        developers_group_id = self.get_gitlab_group_id('developers')
        if developers_group_id is not None:
            param = 'private_token={token}'.format(token=GITLAB_TOKEN.value)
            data = {
                'user_id': self.user_id,
                'access_level': 30  # Developer access
            }
            relative_url = (
                '/groups/{group_id}/members?{param}'
                .format(group_id=developers_group_id, param=param)
            )
            response = self.session.post(
                url=GitLabMain.absolute_url(relative_url), data=data
            )
            try:
                response.raise_for_status()
            except requests.HTTPError:
                self.errors.append(
                    'An error occurred while adding to gitlab developers team'
                    '. Please contact community!'
                )
        else:
            self.errors.append(
                'No developers group found on gitlab! Please report this to'
                ' the organization.'
            )

    def invite_to_developers_team(self):
        """
        Send an invite to the contributor to join developers team of the
        organization
        """
        developers_team_id = self.get_github_team_id('{} developers'.format(
            ORG_NAME
        ))
        if developers_team_id is not None:
            relative_url = '/teams/{team_id}/memberships/{username}'.format(
                team_id=developers_team_id, username=self.username
            )
            get_response(
                self.session.put, GitHubMain.absolute_url(relative_url),
                GITHUB_TOKEN.auth
            )
        else:
            self.errors.append(
                'No developers group found on github! Please report this to'
                ' the organization.'
            )

    def get_gitlab_group_id(self, group_name):
        """
        :param group_name: Name of the GitLab group
        :return: The group-id of that GitLab group
        """
        group_search_url = '/groups?per_page=100&search={}'.format(group_name)
        absolute_url = GitLabMain.absolute_url(group_search_url)
        next_url = True
        results = list()
        while next_url:
            r_json, other_data = get_response(
                self.session.get, absolute_url, GITLAB_TOKEN.auth
            )
            results.extend(r_json)
            if 'next' in other_data.keys():
                absolute_url = other_data.get('next').get('url')
            else:
                next_url = False
        for result in results:
            if (result['name'] == group_name
                    and result['full_path'].__contains__(ORG_NAME)):
                return result['id']
        return None

    def get_github_team_id(self, team_name):
        """
        :param team_name: Name of the GitHub team within an organization
        :return: The team-id of that GitHub team
        """
        org_teams = GitHub.get(
            GITHUB_TOKEN, GitHubMain.absolute_url('/orgs/{}/teams'.format(
                ORG_NAME
            ))
        )
        for team in org_teams:
            if team['name'] == team_name:
                return team['id']
        return None

    def get_error_messages(
            self, linter_usage_in_project, newcomer_contributions,
            low_level_contributions
    ):
        """
        :param linter_usage_in_project: Boolean, whether the project has
        added the .coafile in it or not, with the CI status green
        :param newcomer_contributions: Boolean, whether the contributor has
        worked on newcomer issue or not, and has reviewed a newcomer
        merge_request or not
        :param low_level_contributions: Boolean, whether the contributor has
        worked on low level issue or not, and has reviewed a low level
        difficulty merge_request or not
        """
        if not linter_usage_in_project:
            self.errors.append('Please provide a valid project web url!')
            self.errors.append('Make sure the CI build status is Green!')
            self.errors.append('Please provide a valid build file relative'
                               ' path ending with .yml extension.')
            self.errors.append('The .coafile should have minimum 5'
                               ' sections with all valid bears mentioned.')
            self.errors.append(
                'Please provide a valid CMD line output relative path,'
                ' containing the output of {org} which when executed over'
                ' the project. Also, the uploaded filename should end with'
                ' .sh (the bash) file extension'.format(org=ORG_NAME)
            )
        if not newcomer_contributions:
            self.errors.append(
                'Make sure you provide correct web url of newcomer issue'
                ' solved by you without much assistance. The issue must'
                ' be closed, has a difficult/newcomer label and you not'
                ' have asked for much assistance.'
            )
            self.errors.append(
                'Make sure you provide a correct web url of merge request'
                ' created by you, that solves a newcomer issue. The commit'
                ' should reference the newcomer issue, in such a manner that'
                ' it closes the issue, when merged. Also, you must have not'
                ' received much assistance while solving the issue.'
            )
            self.errors.append(
                'Make sure you provide a correct web url of merge request'
                ' reviewed by you. And that review should not be a approved'
                ' review or just a comment.'
            )
        if not low_level_contributions:
            self.errors.append(
                'Make sure you provide correct web url of low difficulty'
                ' level issue solved by you. The issue must be closed,'
                ' and has a difficult/low label.'
            )
            self.errors.append(
                'Make sure you provide a correct web url of merge request'
                ' created by you, that solves a low difficulty level issue.'
                ' And you must not be redirected to documentation by any'
                ' contributor.'
            )
            self.errors.append(
                'Make sure you provide a correct web url of merge request'
                ' reviewed by you, which solves a low difficulty level issue.'
                ' And that review should not be a approved review or comment.'
                ' Also, no additional problems is being raised by anyone else'
                ' once you reviewed. Do review carefully! So that no-one else'
                ' find extra problems other than the problems raised by you.'
            )

    def prepare_and_send_email(self, validation_checks_info: tuple):
        """
        Get contributor email with the email subject and its body-message.
        :param validation_checks_info: A tuple of 4 values, where 1st value
        indicates whether the hoster accounts are valid or not, 2nd one
        indicates whether the contrib exists or not, 3rd one indicates whether
        the contributor is already a developer or not
        :return: None if the accounts are not valid, And Exit status, if
         EMAIL-BACKEND environment variables not set
        """
        valid_hoster_accounts = validation_checks_info[0]
        contrib_exists = validation_checks_info[1]
        is_a_developer = validation_checks_info[2]
        if valid_hoster_accounts:
            email = self.github_user_data['email']
            message, subject = self.get_email_body_and_subject(
                contrib_exists, is_a_developer
            )
            return send_email(subject, email, message)
        return

    def get_email_body_and_subject(self, contrib_exists, is_a_developer):
        """
        :param contrib_exists: Contributor exists or not
        :param is_a_developer: Contributor is a member of developers team or
         not
        :return: The email body and the subject
        """
        subject = '{}: Request to get promoted denied!'.format(ORG_NAME)
        if not contrib_exists or is_a_developer:
            message = (
                'Hi {username},\n{error} Therefore, your submission has been '
                'denied!'.format(username=self.username, error=errors[0])
            )
        elif errors:
            message = (
                "Hi {username},\nWe couldn't accept your '{org} newcomer"
                " promotion' request as there were some validation errors."
                ' Please correct the following errors and re-apply to become'
                ' a developer. Make sure you have not applied for promotion'
                ' more than 3 times. If you do so, you will be blacklisted'
                ' and would not be apply to again. Following validation'
                ' errors occurred-\n'.format(username=self.username,
                                             org=ORG_NAME)
            )
            for index, error in enumerate(self.errors):
                message += '{i}: {err}\n'.format(i=index + 1, err=error)
            message = self.add_submitted_data(message)
        else:
            subject = (
                '{org}: Request to get promoted to developers role accepted!'
                .format(org=ORG_NAME)
            )
            message = (
                'Hi {username},\nCongratulations! You have completed the'
                ' newcomer process and you are eligible to be a member of'
                ' {org} developers team. Please accept the GitHub invite'
                ' sent. And you have also been added to GitLab {org}'
                ' developers team too. Congratulations once again! We expect'
                ' from you that you will keep on making great contributions'
                ' to the {org}, and will make correct use of this developer'
                ' role.'.format(username=self.username, org=ORG_NAME)
            )
        message += "\nEnjoy Coding 'n' Learning\n"
        return message, subject

    def add_submitted_data(self, message):
        message += '\nYou submitted following information:\n'
        project_details = self.user_data['project_details']
        message += 'Project Details:\n{details}'.format(
            details='\n'.join([
               '\t{k}: {v}'.format(k=key, v=value)
                    for key, value in project_details.items()
            ])
        )
        newcomer_issue = self.user_data['newcomer_issue']
        message += 'Newcomer Issue Details:\n{details}'.format(
            details='\n'.join([
               '\t{k}: {v}'.format(k=key, v=value)
                    for key, value in newcomer_issue.items()
            ])
        )
        low_level_issue = self.user_data['low_level_issue']
        message += 'Low difficulty level issue details:\n{details}'.format(
            details='\n'.join([
                '\t{k}: {v}'.format(k=key, v=value)
                for key, value in low_level_issue.items()
            ]))
        return message


@lru_cache(maxsize=32)
def fetch_contributors_request():
    form_name = os.environ.get('NEWCOMER_PROMOTION_REQUEST_FORM_NAME')

    if not form_name:
        logging.error('Newcomer promotion request form name not provided!'
                      ' Please set environment variable NEWCOMER_PROMOTION'
                      '_REQUEST_FORM_NAME.')
        exit()

    submissions = NETLIFY.get_submissions(form_name)
    for entry in submissions:
        newcomer = PromoteNewcomer(entry=entry, form_id=entry['id'],
                                   form_name=form_name)
        validation_checks_information = newcomer.validate_submitted_data()
        newcomer.prepare_and_send_email(validation_checks_information)
        NETLIFY.delete_submission(entry['id'])
