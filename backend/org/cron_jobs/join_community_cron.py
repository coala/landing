import os

import requests
import urllib
from datetime import datetime

import logging
from functools import lru_cache

from IGitt.GitHub import GitHub
from IGitt.GitLab import GitLab
from IGitt.GitHub.GitHub import GitHub as GitHubMain
from IGitt.GitLab.GitLab import GitLab as GitLabMain
from IGitt.Interfaces import get_response
from IGitt.GitHub.GitHubUser import GitHubUser
from IGitt.GitLab.GitLabUser import GitLabUser, GitLabPrivateToken

from coala_web.settings import ORG_NAME, NETLIFY, GITLAB_API_KEY
from org.git import get_igitt_org, GITHUB_TOKEN
from org.models import Contributor
from utilities import send_email

GITHUB_ORG = get_igitt_org('github')
GITLAB_ORG = get_igitt_org('gitlab')
GITLAB_TOKEN = GitLabPrivateToken(GITLAB_API_KEY)
GH_BASE_URL = 'https://github.com/'
GL_BASE_URL = 'https://gitlab.com/'


class InviteNewcomer:

    def __init__(self, entry, form_id, form_name):
        """
        :param entry_data: Data submitted by user when requested to join org
        :param form_id: Unique ID of JOIN_COMMUNITY_FORM_NAME
        :param form_name: JOIN_COMMUNITY_FORM_NAME
        """
        self.form_id = form_id
        self.form_name = form_name
        self.user_data = self.get_submitted_data(entry)
        self.gh_user_data = None
        self.gl_user_data = None
        self.username = None
        self.user_id = None
        self.session = requests.Session()

    def get_submitted_data(self, entry):
        """
        :param data: Submitted data by user
        :return: A dict of information to be used in-program
        """
        data = entry['data']
        return {
            'instance': {
                'github': GitHubUser(GITHUB_TOKEN, data['github_username']),
                'gitlab': GitLabUser(GITLAB_TOKEN, data['gitlab_user_id'])
            },
            'github': {
                'username': data['github_username'],
                'first_repo': data['gh_first_repo'],
                'training_repo': data['gh_git_training_exercise'],
                'most_contributed_repo': data['gh_most_contributed_repo']
            },
            'gitlab': {
                'user_id': data['gitlab_user_id'],
                'first_repo_id': data['gl_first_repo_id'],
                'training_repo_id': data['gl_git_training_exercise'],
                'most_contributed_repo_id': data[
                    'gl_most_contributed_repo_id'
                ]
            },
            'submitted_on': datetime.strptime(entry['created_at'],
                                              '%Y-%m-%dT%H:%M:%S.%fZ'),
        }

    def validate_submitted_data(self):
        """
        Check whether the contributor already exists or not! And, if exists
        validate whether the contributor is eligible to be a member of org
        or not by performing some checks!
        :return: Whether the hoster accounts are valid or not, contrib exists
         or not, with a list of validation errors, if any
        """
        contrib = Contributor.objects.filter(login=self.username)
        valid_hoster_accounts, errors = self.accounts_have_same_username()
        if not contrib.exists():
            if valid_hoster_accounts:
                return True, False, self.validate_newcomer()
            else:
                return False, False, errors
        else:
            return valid_hoster_accounts, True, [
                "You're already a member of the organization!"
            ]

    def accounts_have_same_username(self):
        gh_instance = self.user_data['instance']['github']
        gl_instance = self.user_data['instance']['gitlab']
        errors = list()
        try:
            self.gh_user_data = gh_instance.data.get()
            self.username = self.gh_user_data['login']
        except RuntimeError:
            errors.append('Provided GitHub username is invalid! There is no'
                          ' such account associated with this username.')
        try:
            gl_instance.data.refresh()
            self.gl_user_data = gl_instance.data.get()
            self.user_id = self.gl_user_data['id']
            if self.username != self.gl_user_data['username']:
                errors.append('The GitLab account must be created using'
                              ' GitHub! Please Sign up for GitLab using'
                              ' GitHub, as it is mandatory to have same'
                              ' username.')
        except RuntimeError:
            errors.append('Provided GitLab user-id is invalid! There is no'
                          ' such account associated with this user-id.')
        if errors:
            return False, errors
        return True, errors

    def validate_newcomer(self):
        """
        Check whether the data provided by newcomer is valid or not! And,
        if valid, send invite to the newcomer to join the organization else
        log it.
        :return: Validation errors, if any.
        """
        valid_newcomer, errors = self.perform_validation_checks()
        if valid_newcomer:
            errors = self.send_invite_to_contributor()
        if errors:
            logging.error('Submission {form_id} not saved for form'
                          ' {form_name}.'.format(form_id=self.form_id,
                                                 form_name=self.form_name))
        return errors

    def perform_validation_checks(self):
        """
        Perform validation checks to validate the newcomer data. And, get the
        error messages depending upon the validation checks values
        :return: Whether the newcomer is valid or not, with validation errors
        """
        account_created_recently = self.has_created_account_recently()
        pushed_code = self.has_pushed_code()
        done_training = self.had_done_git_training()
        contributed_earlier = self.has_contributed_earlier()
        newcomer_valid = any([
            all([account_created_recently, pushed_code, done_training]),
            contributed_earlier
        ])
        errors = self.get_error_messages(
            account_created_recently, pushed_code, done_training,
            contributed_earlier
        )
        return newcomer_valid, errors

    def has_created_account_recently(self):
        """
        Check whether the contributor has created account recently or not.
        If the difference between form-submission and account creation is
        more than 60 minutes, then True else False
        :return: Boolean
        """
        gh_acc_created_at = datetime.strptime(self.gh_user_data['created_at'],
                                              '%Y-%m-%dT%H:%M:%SZ')
        github_account_created_recently = self.check_account_created_time(
            gh_acc_created_at, self.user_data['submitted_on']
        )

        gl_acc_created_at = datetime.strptime(self.gl_user_data['created_at'],
                                              '%Y-%m-%dT%H:%M:%S.%fZ')
        gitlab_account_created_recently = self.check_account_created_time(
            gl_acc_created_at, self.user_data['submitted_on']
        )

        return (github_account_created_recently or
                gitlab_account_created_recently)

    def check_account_created_time(self, date1, date2):
        """
        :param date1: Account created at datetime
        :param date2: Form Submitted on datetime
        :return: Boolean
        """
        datetime_difference = date2 - date1
        if datetime_difference.seconds > 3600:
            return True
        return False

    def has_pushed_code(self):
        """
        Check whether the contributor has pushed any code to its
        user space or not!
        :return: Boolean
        """
        pushed_code_to_github = self.verify_pushed_code('github')
        pushed_code_to_gitlab = self.verify_pushed_code('github')
        return pushed_code_to_github or pushed_code_to_gitlab

    def verify_pushed_code(self, hoster):
        """
        Check whether the contributor has pushed any code to its
        user space or not! Conditioned that, the repository should
        be owned by contrib, not forked and has atleast 3 pull-requests
        merged to repo, authored by that contrib.
        :param hoster: Either github or gitlab value
        :return: Boolean
        """
        if hoster == 'github':
            first_repo_url = self.user_data['github']['first_repo']
            repository = self.get_repo_details('github',
                                               gh_repo_url=first_repo_url)
            if repository is None:
                return False
            owner = repository['owner']['login']
            if (owner == self.gh_user_data['login']
                    and not repository['fork']
                    and self.check_commit_count('github',
                                                repo_url=repository,
                                                commits_count=3)):
                return True
            return False
        else:
            repo_id = self.user_data['gitlab']['first_repo_id']
            repository = self.get_repo_details('gitlab',
                                               gl_project_id=repo_id)
            if repository is None:
                return False
            owner_id = repository['owner']['id']
            if (owner_id == self.gl_user_data['id']
                    and 'forked_from_project' not in repository.keys()
                    and self.check_commit_count('gitlab',
                                                project_id=repo_id,
                                                commits_count=3)):
                return True
            return False

    def get_repo_details(self, hoster, gh_repo_url=None, gl_project_id=None):
        """
        :param hoster: Either github or gitlab value
        :param gh_repo_url: The GitHub repository url
        :param gl_project_id: The GitLab project identifier
        :return: JSON response about the repository details
        """
        if hoster == 'github':
            repo_name = gh_repo_url.replace(GH_BASE_URL, '')
            url = GitHubMain.absolute_url('/repos/{name}'.format(
                name=repo_name
            ))
            try:
                return GitHub.get(GITHUB_TOKEN, url)
            except RuntimeError:
                return None
        else:
            relative_url = '/projects/{id}'.format(id=gl_project_id)
            url = GitLabMain.absolute_url(relative_url)
            try:
                return GitLab.get(GITLAB_TOKEN, url)
            except RuntimeError:
                return None

    def check_commit_count(self, hoster, repo_url=None, project_id=None,
                           commits_count=0):
        """
        Check whether the contributor has done a number of commits in
        the provided repository/project
        :param hoster: Either github or gitlab value
        :param repo_url: GitHub repository URL
        :param project_id: GitLab project identifier
        :param commits_count: Number of commits done in that
         repository
        :return: Boolean
        """
        if hoster == 'github':
            repo_name = repo_url.replace(GH_BASE_URL, '')
            username = self.user_data['github']['username']
            query_params = 'author={username}'.format(username=username)
            relative_url = '/repos/{name}/commits?{params}'.format(
                name=repo_name, params=query_params
            )
            commits_url = GitHubMain.absolute_url(relative_url)
            try:
                response = GitHub.get(GITHUB_TOKEN, commits_url)
            except RuntimeError:
                return False
            if len(response) >= commits_count:
                return True
            return False
        else:
            commit_author_email = self.gl_user_data['public_email']
            user_commit_count = 0
            query_params = 'per_page=100'
            relative_url = '/projects/{id}/repository/commits/?'.format(
                id=project_id
            )
            search_url = GitLabMain.absolute_url(relative_url + query_params)
            try:
                r_json, other_data = get_response(
                    self.session.get, search_url, GITLAB_TOKEN.auth
                )
            except RuntimeError:
                return False
            for commit in r_json:
                if commit['author_email'] == commit_author_email:
                    user_commit_count += 1
                    if user_commit_count >= commits_count:
                        return True
            return False

    def check_contributions(self, hoster, repo_url=None, project_id=None,
                            contributions_count=0):
        """
        Check whether the contributed has done a number of contribution in
        the provided repository/project
        :param hoster: Either github or gitlab
        :param repo_url: GitHub repository url
        :param project_id: GitLab project identifier
        :param contributions_count: Number of contributions done in that
         repository
        :return: Boolean
        """
        if hoster == 'github':
            repo_name = repo_url.replace(GH_BASE_URL, '')
            username = self.gh_user_data['login']
            query_params = 'author:{} type:pr is:merged repo:{}'.format(
                username, repo_name
            )
            encoded_params = urllib.parse.quote(query_params)
            search_url = GitHubMain.absolute_url('/search/issues?q='
                                                 + encoded_params)
            try:
                r_json, other_data = get_response(
                    self.session.get, search_url, GITHUB_TOKEN.auth
                )
            except RuntimeError:
                return False
            if r_json.get('total_count') >= contributions_count:
                return True
            return False
        else:
            user_id = self.gl_user_data['id']
            query_params = 'author_id={}&state=merged&per_page=100'.format(
                user_id
            )
            relative_url = '/projects/{id}/merge_requests/?'.format(
                id=project_id
            )
            search_url = GitLabMain.absolute_url(relative_url + query_params)
            try:
                r_json, other_data = get_response(
                    self.session.get, search_url, GITLAB_TOKEN.auth
                )
            except RuntimeError:
                return False
            if len(r_json) > contributions_count:
                return True
            return False

    def had_done_git_training(self):
        """
        Check whether the contributor has done any git training on
        github or gitlab project
        :return: Boolean
        """
        gh_training_repo = self.user_data['github']['training_repo']
        done_training_on_gh = self.check_contributions(
            'github', repo_url=gh_training_repo, contributions_count=1
        )
        gl_training_repo = self.user_data['gitlab']['training_repo_id']
        done_training_on_gl = self.check_contributions(
            'gitlab', project_id=gl_training_repo, contributions_count=1
        )
        return done_training_on_gh or done_training_on_gl

    def has_contributed_earlier(self):
        """
        Check whether the contributor has earlier to any repository
        which has minimum number of 20 stars count and the contributor
        has minimum 5 contributions to that repository
        :return: Boolean
        """
        gh_repo = self.user_data['github']['most_contributed_repo']
        gh_repo_stars = self.check_stars_count(
            'github', repo_url=gh_repo, star_count=20
        )
        done_contribution_earlier_gh = self.check_contributions(
            'github', repo_url=gh_repo, contributions_count=5
        )

        gl_repo_id = self.user_data['gitlab']['most_contributed_repo_id']
        gl_repo_stars = self.check_stars_count(
            'gitlab', repo_id=gl_repo_id, star_count=20
        )
        done_contribution_earlier_gl = self.check_contributions(
            'gitlab', project_id=gl_repo_id, contributions_count=5
        )
        return (
                (gh_repo_stars and done_contribution_earlier_gh) or
                (gl_repo_stars and done_contribution_earlier_gl)
        )

    def check_stars_count(self, hoster, repo_url=None, repo_id=None,
                          star_count=0):
        """
        Check whether the repository has a given number of stars or not
        :param hoster: Either github or gitlab value
        :param repo_url: GitHub repository url
        :param repo_id: GitLab project identifier
        :param star_count: Minimum number of stars count of repository
        :return: Boolean
        """
        if hoster == 'github':
            repository = self.get_repo_details('github', gh_repo_url=repo_url)
            if repository is None:
                return False
            if repository['stargazers_count'] > star_count:
                return True
            return False
        else:
            repository = self.get_repo_details('gitlab', gl_project_id=repo_id)
            if repository is None:
                return False
            if repository['star_count'] > star_count:
                return True
            return False

    def get_error_messages(self, account_created_recently=False,
                           pushed_code=False, done_training=False,
                           contributed_earlier=False):
        """
        :param account_created_recently: Boolean, whether the account was
         created recently or not
        :param pushed_code: Boolean, whether the contributor has pushed code
         to github or gitlab
        :param done_training: Boolean, whether the contributor has done any
         git training or not
        :param contributed_earlier: Boolean, whether the contributor has
         contributed earlier or not
        :return: A list of validation errors
        """
        possible_errors = list()
        if not account_created_recently:
            possible_errors.append(
                'Please make sure you entered your correct GitHub username and'
                ' GitLab user-id. Also, the accounts which have been created'
                " recently aren't being accepted. There should be a"
                ' difference of at-least 1 hour between the account creation('
                'on GitHub or GitLab) and the Join form.'
            )
        if not pushed_code:
            possible_errors.append(
                "We couldn't verify your pushed code to GitHub or GitLab."
                ' Please make sure that your pushed code(to any of the'
                ' hoster) is not a forked repository, that is, the project'
                ' should be owned by you and the project should have at-least'
                ' 3 commits authored by you And, make sure you have published'
                ' your email publicly.'
            )
        if not done_training:
            possible_errors.append(
                "We couldn't verify that you have done git training on any of"
                ' project provided by you in the form! Please make sure to'
                ' provide the correct details about the repository on which'
                " you've done git training and have at-least 1 contribution"
                ' in that repository.'
            )
        if not contributed_earlier:
            possible_errors.append(
                'A good news for you! If you have done at-least 5'
                ' contributions(merged pull requests) to a project that has'
                ' more than 20 stars than you are eligible of joining the'
                ' community automatically. If you have done such contributions'
                ' to any project, please provide correct repository details'
                ' while filling up the Join form.'
            )
        return possible_errors

    def send_invite_to_contributor(self):
        """
        Send invite to the contributor to join the github newcomers team with
        accepting the access request to organization gitlab newcomers team.
        :return: A list of errors
        """
        errors = list()
        try:
            errors = self.approve_gitlab_access_request()
            errors = self.invite_to_newcomers_team(errors=errors)
        except RuntimeError:
            errors.append("You haven't requested access to {org} newcomers"
                          ' group on GitLab. Please request access to'
                          ' {request_url} and re-apply for joining the'
                          ' community.')
        return errors

    def approve_gitlab_access_request(self):
        """
        Approve the access request, requested by contributor on oragnizations
         GitLab group
        :return: A list of errors
        """
        errors = list()
        newcomers_group_id = self.get_gitlab_group_id('newcomers')
        if newcomers_group_id is not None:
            param = 'private_token={token}'.format(token=GITLAB_TOKEN.value)
            relative_url = (
                '/groups/{group_id}/access_requests/{user_id}/approve?{param}'
                .format(group_id=newcomers_group_id, user_id=self.user_id,
                        param=param)
            )
            get_response(
                self.session.put, GitLabMain.absolute_url(relative_url),
                GITLAB_TOKEN.auth
            )
        else:
            errors.append('No newcomers group found on gitlab! Please report'
                          ' this to the organization.')
        return errors

    def invite_to_newcomers_team(self, errors):
        """
        Send an invite to the contributor to join the organization
        :param errors: Validation errors, if any
        :return: A list of errors
        """
        newcomers_team_id = self.get_github_team_id('{} newcomers'.format(
            ORG_NAME
        ))
        if newcomers_team_id is not None:
            relative_url = '/teams/{team_id}/memberships/{username}'.format(
                team_id=newcomers_team_id, username=self.username
            )
            get_response(
                self.session.put, GitHubMain.absolute_url(relative_url),
                GITHUB_TOKEN.auth
            )
        else:
            errors.append('No newcomers group found on github! Please report '
                          'this to the organization.')
        return errors

    def get_gitlab_group_id(self, group_name):
        """
        :param group_name: Name of the GitLab group
        :return: The group-id of that GitLab group
        """
        groups_url = '/groups?search=' + group_name
        r_json, other_data = get_response(self.session.get,
                                          GitLabMain.absolute_url(groups_url),
                                          GITLAB_TOKEN.auth)
        for result in r_json:
            if (result['name'] == group_name
                    and result['full_path'].__contains__(ORG_NAME)):
                return result['id']
        return None

    def get_github_team_id(self, team_name):
        """
        :param team_name: Name of the GitHub team within an organization
        :return: The team-id of that GitHub team
        """
        org_teams = GitHub.get(
            GITHUB_TOKEN, GitHubMain.absolute_url('/orgs/{}/teams'.format(
                ORG_NAME
            ))
        )
        for team in org_teams:
            if team['name'] == team_name:
                return team['id']
        return None

    def prepare_and_send_email(self, validation_checks_info: tuple):
        """
        Get contributor email with the email subject and its body-message.
        :param validation_checks_info: A tuple of 3 values, where 1st value
        indicates whether the hoster accounts are valid or not, 2nd one
        indicates whether the contrib exists or not and last one is a list
        of errors, if any
        :return: None if the accounts are not valid, And Exit status, if
         EMAIL-BACKEND environment variables not set
        """
        valid_hoster_accounts = validation_checks_info[0]
        contrib_exists = validation_checks_info[1]
        errors = validation_checks_info[2]
        if valid_hoster_accounts:
            email = self.gh_user_data['email']
            message, subject = self.get_email_body_and_subject(contrib_exists,
                                                               errors)
            return send_email(subject, email, message)
        return

    def get_email_body_and_subject(self, contrib_exists, errors):
        """
        :param contrib_exists: Contributor exists or not
        :param errors: Validation errors, if any
        :return: The email body and the subject
        """
        subject = '{}: Request to join community denied!'.format(ORG_NAME)
        if contrib_exists:
            message = (
                'Hi {username},\n{error} Therefore, your submission has been '
                'denied to re-join the organization'.format(
                    username=self.username, error=errors[0]
                )
            )
        elif errors:
            message = (
                "Hi {username},\nWe couldn't accept your 'join {org}"
                " community' request as there were some validation errors."
                ' Please correct the following errors and re-apply to join'
                ' the community. Following validation errors occurred-\n'
                .format(username=self.username, org=ORG_NAME)
            )
            for index, error in enumerate(errors):
                message += '{i}: {err}\n'.format(i=index + 1, err=error)
        else:
            subject = '{org}: Request to join community accepted!'.format(
                org=ORG_NAME
            )
            message = (
                'Hi {username},\nWelcome to {org} organization! Please accept'
                ' the GitHub invite sent. And your GitLab request access to'
                ' newcomers group has also been accepted. Also, Have a look'
                ' at the {org} newcomers guide to get started with'
                ' contributions.'.format(username=self.username, org=ORG_NAME)
            )
        message += "\nEnjoy Coding 'n' Learning\n"
        return message, subject


@lru_cache(maxsize=32)
def fetch_submissions():
    form_name = os.environ.get('JOIN_COMMUNITY_FORM_NAME')

    if not form_name:
        logging.error('Join community netlify form name not provided! Please'
                      ' set environment variable JOIN_COMMUNITY_FORM_NAME.')
        exit()

    submissions = NETLIFY.get_submissions(form_name)
    for entry in submissions:
        newcomer = InviteNewcomer(entry=entry, form_id=entry['id'],
                                  form_name=form_name)
        validation_checks_information = newcomer.validate_submitted_data()
        newcomer.prepare_and_send_email(validation_checks_information)
        NETLIFY.delete_submission(entry['id'])
