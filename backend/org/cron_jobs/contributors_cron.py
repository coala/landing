import json
import datetime
from functools import lru_cache

import urllib
import requests

import logging

from IGitt.GitHub.GitHubUser import GitHubToken
from IGitt.GitHub.GitHub import GitHub as GitHubMain
from IGitt.GitLab.GitLab import GitLab as GitLabMain
from IGitt.GitLab import GitLab
from IGitt.GitLab.GitLabUser import (
    GitLabPrivateToken,
    GitLabOAuthToken
)
from IGitt.GitLab.GitLabOrganization import GitLabOrganization
from IGitt.Interfaces import get_response
from IGitt.GitHub.GitHubRepository import GitHubRepository
from IGitt.GitLab.GitLabRepository import GitLabRepository

from coala_web.settings import GITHUB_API_KEY, GITLAB_API_KEY, ORG_NAME
from org.models import Contributor, ContributorAccessToken
from org.serializers import ContributorSerializer


class UpdateContributorsData:

    def __init__(self, hoster, tokens):
        self.USERNAME = tokens.user.login
        self.TOKENS = tokens
        self.ORG_NAME = ORG_NAME
        self.HOSTER = hoster
        self.GH_TOKEN = None
        self.GL_TOKEN = None
        self.GL_ORG_ID = None
        self.GL_USER_ID = None
        self.since = None
        self.data = None

    def get_saved_data(self):
        """
        Get existing contributor data and store in 'data' instance variable
        """
        contributor = Contributor.objects.get(login=self.USERNAME)
        contrib_serializer = ContributorSerializer(contributor)
        self.data = contrib_serializer.data

    def load_dumped_json_data(self, variables):
        """
        Load the dumped JSON string
        :param variables: a list of data fields which are needed to be loaded
         before working on them
        """
        for variable in variables:
            if self.data[variable] is not None:
                self.data[variable] = json.loads(self.data[variable])

    def convert_none_types_to_dicts(self, variables):
        """
        Convert the NoneType object to dict if None
        :param variables: a list of data fields which are needed to be
         converted
        before working on them
        """
        for variable in variables:
            if self.data[variable] is None:
                self.data[variable] = dict()

    def last_updated_date(self):
        """
        Set the date when the contributor data was last updated
        """
        if self.HOSTER in self.data['updated_at'].keys():
            self.since = self.data['updated_at'][self.HOSTER]

    def add_missing_keys_to_statistics(self, repository, to_be_saved,
                                       year, week, weekday, state=None):
        """
        Add the missing keys to the contributor statistics data
        :param repository: Repository name
        :param to_be_saved: What is to be saved? For example: issues, prs etc.
        :param year: Year in which the object was created, closed, updated or
         merged
        :param week: Week in which the object was created, closed, updated or
         merged
        :param weekday: Weekday in which the object was created, closed,
         updated or merged
        :param state: What is the state of the object which is to be saved?
         For example, opened, closed, merged or assigned
        :return:
        """
        statistics = self.data['statistics']
        hoster = self.HOSTER
        if hoster not in statistics.keys():
            statistics[self.HOSTER] = dict()
        if repository not in statistics[hoster].keys():
            statistics[self.HOSTER][repository] = dict()
        if to_be_saved not in statistics[hoster][repository].keys():
            statistics[hoster][repository][to_be_saved] = dict()
        if state is None:
            repo_stats = statistics[hoster][repository][to_be_saved]
        else:
            if state not in statistics[hoster][repository][to_be_saved].keys():
                statistics[hoster][repository][to_be_saved][state] = dict()
            repo_stats = statistics[hoster][repository][to_be_saved][state]
        if year not in repo_stats.keys():
            repo_stats[year] = dict()
        if week not in repo_stats[year].keys():
            repo_stats[year][week] = dict()
        if weekday not in repo_stats[year][week].keys():
            repo_stats[year][week][weekday] = dict()

    def get_labels(self, issue):
        """
        :param issue: Object having a set of labels
        :return: All the labels with there names and color code
        """
        labels = list()
        for label in issue['labels']:
            if isinstance(label, str):
                label = {'name': label, 'color': None}
            labels.append(
                {'name': label['name'], 'color': label['color']}
            )
        return labels

    def get_repository_name_and_issue_number(self, result, scope):
        """
        :param result: a search result dict having useful information
        including repository_url(or web_url) like info.
        :param scope: Type of the result - issues or merge_requests
        :return: the name of the repository and the issue or pull request
         number
        """
        if self.HOSTER == 'github':
            repo_url = result['repository_url']
            number = result['number']
        else:
            repo_url = result['web_url']
            repo_url, number = repo_url.split('/{}/'.format(scope))
        start_pos = repo_url.find(self.ORG_NAME)
        return repo_url[start_pos:], number

    def get_year_week_weekday(self, date):
        """
        :return: the year, week and weekday of the calendar corresponding to
        the provided date
        """
        try:
            datetime_obj = datetime.datetime.strptime(date,
                                                      '%Y-%m-%dT%H:%M:%SZ')
        except ValueError:
            datetime_obj = datetime.datetime.strptime(date,
                                                      '%Y-%m-%dT%H:%M:%S.%fZ')
        return datetime_obj.isocalendar()

    def get_hoster_search_results(self, **kwargs):
        """
        Gets the API Search URL according to 'HOSTER' instance variable based
        on the **kwargs and then request the HTTP Response for the search URL
        :param kwargs: a dict of query params
        :return: A list of all the paginated results
        """
        if self.HOSTER == 'github':
            search_url = self.get_github_search_url(**kwargs)
            auth = self.GH_TOKEN.auth
        else:
            search_url = self.get_gitlab_search_url(**kwargs)
            auth = self.GL_TOKEN.auth
        session = requests.Session()
        next_url = True
        results = list()
        while next_url:
            r_json, other_data = get_response(session.get, search_url, auth)
            if self.HOSTER == 'github':
                results.extend(r_json.get('items'))
            else:
                results.extend(r_json)
            if 'next' in other_data.keys():
                search_url = other_data.get('next').get('url')
            else:
                next_url = False
        return results

    def get_github_search_url(self, **kwargs):
        """
        :param kwargs: a dict of query params
        :return: GitHub API Search URL with query params
        """
        query_params = ''
        for key, value in kwargs.items():
            query_params += ' {key}:{value}'.format(key=key, value=value)
        encoded_query_params = urllib.parse.quote(query_params)
        return GitHubMain.absolute_url('/search/issues?q=' +
                                       encoded_query_params)

    def get_gitlab_search_url(self, **kwargs):
        """
        :param kwargs: a dict of query params
        :return: GitLab API Search URL with query params
        """
        search_kwargs = kwargs
        search_type = search_kwargs.pop('type')
        query_params = '&'.join([
            '{key}={value}'.format(key=key, value=value)
            for key, value in search_kwargs.items()
        ])
        relative_url = '/groups/{id}/{search_type}/?{params}'.format(
            id=self.GL_ORG_ID, search_type=search_type, params=query_params
        )
        return GitLabMain.absolute_url(relative_url)

    def filter_results(self, results, scope, filter_type):
        """
        :param results: A list of API search results
        :param scope: What is to be filtered? Issues or Pull requests
        :param filter_type: The filter type - opened, closed or merged
        :return: list of filtered results i.e. the results which are not
        yet being counted or stored in contributor statistics
        """
        filtered_results = []
        existing_issues = dict()
        statistics = self.data['statistics'][self.HOSTER]
        for repo_name, stats in statistics.items():
            if scope in stats.keys():
                repository_issues = stats[scope]
                if filter_type in repository_issues.keys():
                    existing_issues[repo_name] = list()
                    issues = repository_issues[filter_type]
                    for year, weeks in issues.items():
                        for week, weekdays in weeks.items():
                            for weekday, issues in weekdays.items():
                                existing_issues[repo_name].append(
                                    issues.keys()
                                )
        if existing_issues:
            for result in results:
                repository, number = self.get_repository_name_and_issue_number(
                    result['web_url'], scope
                )
                if number not in existing_issues[repository]:
                    filtered_results.append(result)
            return filtered_results
        return results

    def save_commits_data(self, pull_requests_key_name):
        """
        Every merged pull request is a commit
        :param pull_requests_key_name: The key name is either 'prs' or
         'merge_requests' depending upon the HOSTER
        """
        statistics = self.data['statistics']
        for repository, stats in statistics[self.HOSTER].items():
            if pull_requests_key_name in stats.keys():
                if 'merged' in stats[pull_requests_key_name].keys():
                    stats['commits'] = stats[pull_requests_key_name]['merged']

    def save_type_of_issues_worked_on(self):
        """
        Save all the assigned issues labels. Gives an idea, about contributor
        that is, on which all types of issues has the contributor worked on!
        """
        statistics = self.data['statistics'][self.HOSTER]
        worked_on_labels = set()
        for repository_stats in statistics.values():
            if 'issues' in repository_stats.keys():
                issues = repository_stats['issues']
                if 'assigned' in issues.keys():
                    assigned_issues = issues['assigned']
                    for year, weeks in assigned_issues.items():
                        for week, weekdays in weeks.items():
                            for weekday, issues in weekdays.items():
                                for issue, labels in issues.items():
                                    for label in labels:
                                        worked_on_labels.add((label['name'],
                                                              label['color']))
        labels_dict = {
            name: color
            for name, color in worked_on_labels
        }
        self.data['type_of_issues_worked_on'][self.HOSTER] = labels_dict

    def get_igitt_hoster_repository(self, repository):
        """
        :param repository: Name of the repository
        :return: an object of IGitt 'GitHub'or 'GitLab' Repository class
        """
        if self.HOSTER == 'github':
            return GitHubRepository(self.GH_TOKEN, repository)
        else:
            return GitLabRepository(self.GL_TOKEN, repository)

    def save_search_results(self, search_results, to_be_saved, state, _at):
        """
        Save all the API Search-Filtered results to the contributor statistics
        :param search_results: a list of hoster API JSON response
        :param to_be_saved: What is to be saved? Issues, PRs, Commits or
         Reviews
        :param state: The state of PRs or Issues? Opened, Closed, Merged,
         or Unmerged
        :param _at: The date type - updated_at, closed_at, or created_at
        """
        for result in search_results:
            repository, number = self.get_repository_name_and_issue_number(
                result, to_be_saved
            )
            date = result[_at]
            year, week, weekday = self.get_year_week_weekday(date)
            self.add_missing_keys_to_statistics(repository, to_be_saved,
                                                year, week, weekday, state)
            statistics = self.data['statistics']
            repo_stats = statistics[self.HOSTER][repository][to_be_saved]
            if state is not None:
                repo_stats = repo_stats[state]
            repo_stats[year][week][weekday][number] = self.get_labels(result)

    def save_working_on_issues_count(self):
        """
        Count the number of issues on which the contributor is still working
        on by the difference of opened pull requests and the assigned issues
        """
        if self.HOSTER == 'gitlab':
            assigned_open_issues_kwargs = {
                'type': 'issues', 'state': 'opened', 'per_page': '100',
                'assignee_id': self.GL_USER_ID
            }
            opened_mrs_kwargs = {
                'type': 'merge_requests', 'state': 'opened',
                'author_id': self.GL_USER_ID, 'per_page': '100'
            }
        else:
            assigned_open_issues_kwargs = {
                'assignee': self.USERNAME, 'type': 'issue', 'state': 'open',
                'org': self.ORG_NAME
            }
            opened_mrs_kwargs = {
                'author': self.USERNAME, 'type': 'pr', 'org': self.ORG_NAME,
                'state': 'open'
            }
        assigned_open_issues = self.get_hoster_search_results(
            **assigned_open_issues_kwargs
        )
        open_mrs = self.get_hoster_search_results(**opened_mrs_kwargs)
        issues_without_prs = len(assigned_open_issues) - len(open_mrs)
        if issues_without_prs < 0:
            issues_without_prs = 0
        self.data['working_on_issues_count'][self.HOSTER] = issues_without_prs

    def save_user_data(self):
        """
        Save all the fetched contributor statistics data to the Contributor
        data model by validating it via a serializer
        """
        contrib = Contributor.objects.get(login=self.USERNAME)
        to_be_dumped = ['statistics', 'type_of_issues_worked_on', 'updated_at',
                        'working_on_issues_count']
        for variable in to_be_dumped:
            self.data[variable] = json.dumps(self.data[variable])
        contrib_serializer = ContributorSerializer(contrib, data=self.data)
        if contrib_serializer.is_valid():
            contrib_serializer.save()
        else:
            logging.error(
                "Couldn't save contributor {} statistics due to following"
                ' errors {}'.format(self.USERNAME, contrib_serializer.errors)
            )


class UpdateGitHubContributorsData(UpdateContributorsData):

    def __init__(self, hoster, tokens):
        super().__init__(hoster, tokens)

    def save_activity(self):
        """
        Contributor Activities to be saved for a given hoster
        """
        self.set_igitt_hoster_instance()
        self.save_issues_data()
        self.save_merge_requests_data()
        self.save_commits_data(pull_requests_key_name='prs')
        self.save_reviews_data()
        self.save_type_of_issues_worked_on()
        self.save_working_on_issues_count()

    def set_igitt_hoster_instance(self):
        """
        Set the GitHub Token instance variable
        """
        github_token = self.TOKENS.github_access_token or GITHUB_API_KEY
        self.GH_TOKEN = GitHubToken(token=github_token)

    def save_issues_data(self):
        """
        Save Opened and Closed issues by contributor with the issues assigned
        to the contributor
        """
        self.save_opened_issues_data()
        self.save_closed_issues_data()
        self.save_assigned_issues_data()

    def save_opened_issues_data(self):
        """
        Save the opened issues by the contributor to the contributor statistics
        data instance variable
        """
        opened_issues_kwargs = {
            'state': 'open', 'type': 'issue', 'org': self.ORG_NAME,
            'author': self.USERNAME
        }
        if self.since:
            opened_issues_kwargs['created'] = '>{}'.format(self.since)
        search_results = self.get_hoster_search_results(**opened_issues_kwargs)
        state = opened_issues_kwargs['state']
        to_be_saved = '{}s'.format(opened_issues_kwargs.get('type'))
        _at = 'created_at'
        self.save_search_results(search_results, to_be_saved, state, _at)

    def save_closed_issues_data(self):
        """
        Save the closed issues by the contributor to the contributor statistics
        data instance variable
        """
        closed_issues_kwargs = {
            'state': 'closed', 'type': 'issue', 'org': self.ORG_NAME,
            'author': self.USERNAME
        }
        if self.since:
            closed_issues_kwargs['closed'] = '>{}'.format(self.since)
        search_results = self.get_hoster_search_results(**closed_issues_kwargs)
        state = closed_issues_kwargs['state']
        to_be_saved = '{}s'.format(closed_issues_kwargs.get('type'))
        _at = 'closed_at'
        self.save_search_results(search_results, to_be_saved, state, _at)

    def save_assigned_issues_data(self):
        """
        Save the assigned issues by the contributor to the contributor
         statistics data instance variable
        """
        assigned_issues_kwargs = {
            'assignee': self.USERNAME, 'type': 'issue', 'org': self.ORG_NAME,
        }
        if self.since:
            assigned_issues_kwargs['updated'] = '>{}'.format(self.since)
        search_results = self.get_hoster_search_results(
            **assigned_issues_kwargs
        )
        assigned_issues = self.filter_results(search_results, 'issues',
                                              'assigned')
        state = 'assigned'
        to_be_saved = '{}s'.format(assigned_issues_kwargs.get('type'))
        _at = 'updated_at'
        self.save_search_results(assigned_issues, to_be_saved, state, _at)

    def save_merge_requests_data(self):
        """
        Save opened, merged and unmerged pull requests by the contributor to
        the statistics data instance variable
        """
        self.save_opened_pull_requests()
        self.save_closed_pull_requests(merge_status='merged')
        self.save_closed_pull_requests(merge_status='unmerged')

    def save_opened_pull_requests(self):
        """
        Save the opened prs by the contributor to the contributor statistics
        data instance variable
        """
        opened_prs_kwargs = {
            'state': 'open', 'type': 'pr', 'org': self.ORG_NAME,
            'author': self.USERNAME
        }
        if self.since:
            opened_prs_kwargs['created'] = '>{}'.format(self.since)
        search_results = self.get_hoster_search_results(**opened_prs_kwargs)
        state = opened_prs_kwargs['state']
        to_be_saved = '{}s'.format(opened_prs_kwargs.get('type'))
        _at = 'created_at'
        self.save_search_results(search_results, to_be_saved, state, _at)

    def save_closed_pull_requests(self, merge_status):
        """
        Save the closed(merged or unmerged) pull requests to the contributor
         statistics data instance variable
        :param merge_status: The state of the merge_requests - 'merged' or
         'unmerged'
        """
        closed_prs_kwargs = {
            'state': 'closed', 'type': 'pr', 'is': merge_status,
            'org': self.ORG_NAME, 'author': self.USERNAME
        }
        if self.since:
            if merge_status == 'merged':
                closed_prs_kwargs['merged'] = '>{}'.format(self.since)
            else:
                closed_prs_kwargs['closed'] = '>{}'.format(self.since)
        search_results = self.get_hoster_search_results(**closed_prs_kwargs)
        state = closed_prs_kwargs['is']
        to_be_saved = '{}s'.format(closed_prs_kwargs.get('type'))
        _at = 'closed_at'
        self.save_search_results(search_results, to_be_saved, state, _at)

    def save_reviews_data(self):
        """
        Save the reviews done by the contributor to the statistics data
         instance variable
        """
        reviews_search_kwargs = {
            'commenter': self.USERNAME, 'type': 'pr', 'org': self.ORG_NAME
        }
        if self.since:
            reviews_search_kwargs['created_at'] = '>{}'.format(self.since)
        search_results = self.get_hoster_search_results(
            **reviews_search_kwargs
        )
        state = None
        to_be_saved = 'reviews'
        _at = 'created_at'
        self.save_search_results(search_results, to_be_saved, state, _at)


class UpdateGitLabContributorsData(UpdateContributorsData):

    def __init__(self, hoster, tokens):
        super().__init__(hoster, tokens)

    def save_activity(self):
        """
        Contributor Activities to be saved for a given hoster
        """
        self.set_igitt_hoster_instance()
        self.save_issues_data()
        self.save_merge_requests_data()
        self.save_commits_data(pull_requests_key_name='merge_requests')
        self.save_type_of_issues_worked_on()
        self.save_working_on_issues_count()

    def set_igitt_hoster_instance(self):
        """
        Set the GitLab Token instance variable either Private or OAuth Token
        """
        if self.TOKENS.gitlab_access_token:
            self.GL_TOKEN = GitLabOAuthToken(self.TOKENS.gitlab_access_token)
        else:
            self.GL_TOKEN = GitLabPrivateToken(GITLAB_API_KEY)
        self.get_gitlab_org_and_user_id()

    def get_gitlab_org_and_user_id(self):
        """
        Set the GitLab Organization ID and User ID instance variables
        """
        gl_org = GitLabOrganization(self.GL_TOKEN, self.ORG_NAME)
        self.GL_ORG_ID = gl_org.data['id']

        search_results = GitLab.get(self.GL_TOKEN,
                                    GitLabMain.absolute_url(
                                        '/users/?username={}'.format(
                                            self.USERNAME))
                                    )
        user = search_results.pop()
        self.GL_USER_ID = user['id']

    def save_issues_data(self):
        """
        Save Opened and Closed issues by contributor with the issues assigned
        to the contributor
        """
        self.save_opened_issues_data()
        self.save_closed_issues_data()
        self.save_assigned_issues_data()

    def save_opened_issues_data(self):
        """
        Save the opened issues by the contributor to the contributor statistics
        data instance variable
        """
        opened_issues_kwargs = {
            'type': 'issues', 'state': 'opened', 'author_id': self.GL_USER_ID,
            'with_labels_details': 'true', 'per_page': '100'
        }
        if self.since:
            opened_issues_kwargs['created_after'] = self.since
        search_results = self.get_hoster_search_results(**opened_issues_kwargs)
        state = opened_issues_kwargs['state']
        to_be_saved = opened_issues_kwargs.get('type')
        _at = 'created_at'
        self.save_search_results(search_results, to_be_saved, state, _at)

    def save_closed_issues_data(self):
        """
        Save the closed issues by the contributor to the contributor statistics
        data instance variable
        """
        closed_issues_kwargs = {
            'type': 'issues', 'state': 'closed', 'author_id': self.GL_USER_ID,
            'with_labels_details': 'true', 'per_page': '100'
        }
        if self.since:
            closed_issues_kwargs['updated_after'] = self.since
        search_results = self.get_hoster_search_results(**closed_issues_kwargs)
        closed_issues = self.filter_results(search_results, 'issues', 'closed')
        state = closed_issues_kwargs['state']
        to_be_saved = closed_issues_kwargs.get('type')
        _at = 'closed_at'
        self.save_search_results(closed_issues, to_be_saved, state, _at)

    def save_assigned_issues_data(self):
        """
        Save the assigned issues by the contributor to the contributor
        statistics data instance variable
        """
        assigned_issues_kwargs = {
           'type': 'issues', 'state': 'opened', 'assignee_id': self.GL_USER_ID,
           'with_labels_details': 'true', 'per_page': '100'
        }
        if self.since:
            assigned_issues_kwargs['updated_after'] = self.since
        search_results = self.get_hoster_search_results(
            **assigned_issues_kwargs
        )
        assigned_issues = self.filter_results(search_results, 'issues',
                                              'assigned')
        state = 'assigned'
        to_be_saved = assigned_issues_kwargs.get('type')
        _at = 'updated_at'
        self.save_search_results(assigned_issues, to_be_saved, state, _at)

    def save_merge_requests_data(self):
        """
        Save opened, merged and closed pull requests by the contributor to
        the statistics data instance variable
        """
        self.save_opened_pull_requests()
        self.save_closed_pull_requests(merge_status='closed')
        self.save_closed_pull_requests(merge_status='merged')

    def save_opened_pull_requests(self):
        """
        Save the opened prs by the contributor to the contributor statistics
        data instance variable
        """
        opened_prs_kwargs = {
            'type': 'merge_requests', 'state': 'opened',
            'author_id': self.GL_USER_ID, 'per_page': '100'
        }
        if self.since:
            opened_prs_kwargs['created_after'] = self.since
        search_results = self.get_hoster_search_results(**opened_prs_kwargs)
        state = opened_prs_kwargs['state']
        to_be_saved = opened_prs_kwargs.get('type')
        _at = 'created_at'
        self.save_search_results(search_results, to_be_saved, state, _at)

    def save_closed_pull_requests(self, merge_status):
        """
        Save the closed or merged pull requests to the contributor
         statistics data instance variable
        :param merge_status: The state of the merge_requests - 'merged' or
         'closed'
        """
        closed_prs_kwargs = {
            'type': 'merge_requests', 'state': merge_status,
            'author_id': self.GL_USER_ID, 'per_page': '100'
        }
        if self.since:
            closed_prs_kwargs['updated_after'] = self.since
        search_results = self.get_hoster_search_results(**closed_prs_kwargs)
        state = merge_status
        to_be_saved = closed_prs_kwargs.get('type')
        _at = '{}_at'.format(merge_status)
        filtered_results = self.filter_results(search_results, to_be_saved,
                                               state)
        self.save_search_results(filtered_results, to_be_saved, state, _at)


@lru_cache(maxsize=32)
def update_contributors():
    access_tokens = ContributorAccessToken.objects.all()
    hosters = ['github', 'gitlab']
    variables = ['statistics', 'type_of_issues_worked_on', 'updated_at',
                 'working_on_issues_count']
    for access_token in access_tokens:
        for hoster in hosters:
            user = get_user_instance(hoster, access_token)
            user.get_saved_data()
            user.load_dumped_json_data(variables)
            user.convert_none_types_to_dicts(variables)
            user.last_updated_date()
            user.save_activity()
            datetime_obj = datetime.datetime.now()
            user.data['updated_at'][hoster] = datetime_obj.strftime(
                '%Y-%m-%dT%H:%M:%SZ'
            )
            user.save_user_data()


def get_user_instance(hoster, access_token):
    if hoster == 'github':
        return UpdateGitHubContributorsData(hoster, access_token)
    elif hoster == 'gitlab':
        return UpdateGitLabContributorsData(hoster, access_token)
