import os
from datetime import datetime

from functools import lru_cache

import logging

from coala_web.settings import NETLIFY
from org.serializers import FeedbackSerializer


@lru_cache(maxsize=32)
def feedback_form_submissions():
    form_name = os.environ.get('FEEDBACK_FORM_NAME')
    if not form_name:
        logging.error('Feedback form name not provided!'
                      'Please set environment variable FEEDBACK_FORM_NAME.')
        exit()
    else:
        # Get the feedback form submissions using NETLIFY API
        users_feedback = NETLIFY.get_submissions(form_name)
        # Save users feedback
        for entry in users_feedback:
            user_ = entry['data']
            data = {
                'username': user_['username'],
                'feedback': user_['feedback'],
                'date': datetime.strptime(entry['created_at'],
                                          '%Y-%m-%dT%H:%M:%S.%fZ'),
                'experience': user_['experience']
            }
            feedback = FeedbackSerializer(data=data)
            if feedback.is_valid():
                feedback.save()
            else:
                logging.error('Submission {form_id} not saved for form'
                              '{form_name}.'.format(form_id=entry['id'],
                                                    form_name=form_name))
            # Delete the form entry as it has been processed by webservices.
            # Avoid re-processing in the next GET request.
            NETLIFY.delete_submission(entry['id'])
