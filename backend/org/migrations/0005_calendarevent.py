# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2019-06-03 11:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('org', '0004_feedback'),
    ]

    operations = [
        migrations.CreateModel(
            name='CalendarEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.TextField(max_length=300, verbose_name='Event title')),
                ('description', models.TextField(max_length=1000, null=True, verbose_name='Event description')),
                ('start_date_time', models.DateTimeField(verbose_name='Event occurrence date and time(in UTC)')),
                ('end_date_time', models.DateTimeField(null=True, verbose_name='Event end date and time(in UTC)')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='org.Contributor')),
            ],
        ),
    ]
