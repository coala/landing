from django.apps import AppConfig
from django.core.cache import cache as django_cache

from IGitt.Utils import Cache


class OrgConfig(AppConfig):
    name = 'org'

    def ready(self):
        # Initialize the IGitt Cache management.
        Cache.use(django_cache.get, django_cache.set)
