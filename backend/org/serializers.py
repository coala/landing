import requests

from rest_framework import serializers

from org.models import (
    CalendarEvent,
    Feedback, Contributor,
    Feedback, Contributor,
    Team, GSOCStudent, Mentor
)
from coala_web.settings import ORG_NAME


class FeedbackSerializer(serializers.ModelSerializer):

    class Meta:
        model = Feedback
        fields = '__all__'


class CalendarEventsSerializer(serializers.ModelSerializer):

    class Meta:
        model = CalendarEvent
        fields = '__all__'

    def validate_user(self, username):
        user_teams = username.teams.all().count()
        if user_teams == 1:
            raise serializers.ValidationError(
                "{} isn't a member of developer team.".format(username.login)
            )
        return username

    def validate_title(self, value):
        if len(value.strip()) < 10:
            raise serializers.ValidationError(
                'The calendar event title should be more than 10 characters.'
            )
        return value.strip()

    def validate_description(self, value):
        return value.strip()


class OrgTeamsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Team
        fields = '__all__'


class GSOCStudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = GSOCStudent
        exclude = ('id', 'valid')

    def validate_cEP(self, value):
        r = requests.get(value)
        if r.status_code == 404:
            raise serializers.ValidationError('Please provide a valid cEP URL')
        return value

    def validate_mentors(self, mentors):
        for mentor in mentors:
            if mentor.teams.all().count() == 1:
                raise serializers.ValidationError('Invalid mentor {} provided'
                                                  ''.format(mentor.login))
        return mentors


class ContributorSerializer(serializers.ModelSerializer):
    teams = OrgTeamsSerializer(many=True, read_only=True)

    class Meta:
        model = Contributor
        fields = '__all__'


class MentorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mentor
        fields = '__all__'

    def validate_user(self, value):
        gsoc_student = self.is_gsoc_student(value)
        developer = self.is_member(value, '{} developers'.format(ORG_NAME))
        maintainer = self.is_member(value, '{} maintainers'.format(ORG_NAME))
        if maintainer or (gsoc_student and developer):
            return value
        errors = []
        if not gsoc_student:
            errors.append('Not a valid GSoC Student!')
        if not developer:
            errors.append('Not a member of the {} developers group!'.format(
                ORG_NAME
            ))
        raise serializers.ValidationError('And '.join(errors))

    def is_gsoc_student(self, user):
        students = GSOCStudent.objects.filter(user=user)
        if students.exists():
            return True
        return False

    def is_member(self, user, team_name):
        filter_teams = user.teams.filter(name=team_name)
        if filter_teams.exists():
            return True
        return False
