import os
import re
import json

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django_common.http import HttpResponse
from rest_framework import views
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from IGitt.GitHub.GitHubIssue import GitHubIssue
from IGitt.GitLab import GitLab
from IGitt.GitLab.GitLab import GitLab as GitLabMain
from IGitt.GitLab.GitLabIssue import GitLabIssue
from IGitt.GitLab.GitLabUser import (
    GitLabUser,
    GitLabOAuthToken,
    GitLabPrivateToken
)
from IGitt.GitHub.GitHubRepository import GitHubRepository
from IGitt.GitLab.GitLabRepository import GitLabRepository
from IGitt.GitHub.GitHubUser import GitHubUser, GitHubToken
from igitt_django.models import IGittIssue, IGittMergeRequest, IGittRepository
from brake.decorators import ratelimit

from coala_web.settings import (
    ORG_NAME,
    GITHUB_API_KEY,
    GITLAB_API_KEY,
)
from .serializers import (
    FeedbackSerializer,
    CalendarEventsSerializer,
    OrgTeamsSerializer,
    GSOCStudentSerializer,
    ContributorSerializer,
    MentorSerializer
)
from org.models import (
    Contributor, Feedback, CalendarEvent,
    Team, InactiveIssue, UnassignedIssue,
    GSOCStudent, ContributorAccessToken, Mentor,
    AssignIssueRequest
)
from org.cron_jobs.issues_cron import save_issue, save_repository
from org.newcomers import active_newcomers

GITHUB_TOKEN = GitHubToken(GITHUB_API_KEY)
GITLAB_TOKEN = GitLabPrivateToken(GITLAB_API_KEY)


class ContributorsAPIView(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = Contributor.objects.all()
    serializer_class = ContributorSerializer

    def get(self, request, *args, **kwargs):
        username = self.request.query_params.get('username')
        if username:
            contrib = self.queryset.filter(login=username)
            if contrib.exists():
                data = ContributorSerializer(contrib.first()).data
                variables = ['statistics', 'type_of_issues_worked_on',
                             'updated_at', 'working_on_issues_count']
                for variable in variables:
                    if data[variable] is not None:
                        data[variable] = json.loads(data[variable])
                return Response(data)
            else:
                return Response({'data': "{} doesn't exist!".format(username)},
                                status.HTTP_404_NOT_FOUND)
        return Response(
            ContributorSerializer(self.get_queryset(), many=True).data
        )


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def issues(request, hoster):
    issues = IGittIssue.objects.all()
    issues = [issue.data for issue in issues
              if issue.repo.hoster == hoster]
    # param default=str is used to dump the datetime object into string.
    return HttpResponse(
        json.dumps(issues, indent=4, default=str),
        content_type='application/json',)


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def merge_requests(request, hoster):
    mrs = IGittMergeRequest.objects.all()
    mrs = [mr.data for mr in mrs
           if mr.repo.hoster == hoster]
    # param default=str is used to dump the datetime object into string.
    return HttpResponse(
        json.dumps(mrs, indent=4, default=str),
        content_type='application/json',)


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def newcomers_active(request):
    newcomers_list = active_newcomers()
    active_newcomers_list = []
    for newcomer in newcomers_list:
        active_newcomers_list.append({'username': newcomer})
    return HttpResponse(
        json.dumps(active_newcomers_list),
        content_type='application/json')


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def org_activity(request):
    issues_file_path = os.path.join(os.getcwd(), 'org_activity.json')
    if os.path.isfile(issues_file_path):
        with open(issues_file_path) as f:
            data = json.load(f)
    else:
        data = {
            'message': 'org_activity.json file not found',
            'status': 500,
        }
    return HttpResponse(json.dumps(data), content_type='application/json')


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def get_inactive_issues(request):
    issues = InactiveIssue.objects.all()
    issues_list = []
    for issue in issues:
        issues_list.append(issue.issue_id.data)
    return HttpResponse(
        json.dumps(issues_list),
        content_type='application/json')


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def get_unassigned_issues(request):
    issues = UnassignedIssue.objects.all()
    issues_list = []
    for issue in issues:
        issues_list.append(issue.issue_id.data)
    return HttpResponse(
        json.dumps(issues_list),
        content_type='application/json')


class FeedbackListView(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer


class CalendarEventsListView(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = CalendarEvent.objects.all()
    serializer_class = CalendarEventsSerializer


class OrganizationTeams(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = Team.objects.all()
    serializer_class = OrgTeamsSerializer


class GSOCStudentsList(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = GSOCStudentSerializer

    def get_queryset(self):
        return GSOCStudent.objects.filter(valid=True)


class AddGSoCStudent(generics.RetrieveAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = GSOCStudent.objects.all()
    serializer_class = GSOCStudentSerializer

    def get_object(self):
        id = self.kwargs.get('id')
        student = GSOCStudent.objects.get(id=id)
        student.valid = True
        student.save()
        return student


class AccessTokenValidator(generics.RetrieveAPIView):
    permission_classes = []
    authentication_classes = []
    OAUTH_PROVIDER = None
    ACCESS_TOKEN = None

    def get(self, request, *args, **kwargs):
        self.OAUTH_PROVIDER = kwargs.get('oauth_provider')
        self.ACCESS_TOKEN = kwargs.get('access_token')
        # Validate whether the Access Token is valid or not, can be validated
        # by getting the user details using the Access Token
        data = self.validate_user()
        return Response(data)

    def validate_user(self):
        """
        Validate whether the user is valid or not
        :return: A boolean
        """
        try:
            username = self.get_username()
        except RuntimeError:
            return self.get_data(is_valid=False, username=None,
                                 message=self.get_message(token=True))
        return self.is_organization_member(username)

    def get_username(self):
        """
        Get the GitHub username using the access token
        :return: the GitHub/GitLab username
        """
        if self.OAUTH_PROVIDER == 'github':
            user_obj = GitHubUser(GitHubToken(self.ACCESS_TOKEN))
        elif self.OAUTH_PROVIDER == 'gitlab':
            user_obj = GitLabUser(GitLabOAuthToken(self.ACCESS_TOKEN))
        return user_obj.username

    def get_data(self, is_valid, username, message):
        return {
            'valid': is_valid,
            'user': username,
            'message': message
        }

    def get_message(self, valid=False, invalid=False, token=False):
        """
        A message (or say, an error message) which explains the validity
        of the token. Whether the user is valid or not
        :param valid: A boolean, whether the user is valid or not
        :param invalid: A boolean, whether the user is invalid or not
        :param token: A boolean, whether the token is valid or not
        :return: the message
        """
        if valid:
            return 'A valid user!'
        if invalid:
            return ("You're not a valid member of organization. Please join"
                    ' the community to Sign-In, or If you have joined,'
                    " please read the Newcomers' guide. Thanks!")
        if token:
            return 'An invalid access-token!'

    def is_organization_member(self, username):
        """
        Check whether the user is the member of the organization or not!
        :param username: The GitHub username
        :return: A dict representing the structure whether the user is valid
        or not
        """
        contrib_objs = Contributor.objects.filter(login=username)
        if contrib_objs.exists():
            contributor = contrib_objs.first()
            self.save_user_token(contributor)
            contributor.oauth_completed = True
            contributor.save()
            return self.get_data(is_valid=True, username=username,
                                 message=self.get_message(valid=True))
        else:
            return self.get_data(is_valid=False, username=None,
                                 message=self.get_message(invalid=True))

    def save_user_token(self, contributor):
        """
        Save the Access Token to the database, which will be used for fetching
        the contributor contributions detail
        :param contributor: An instance of Contributor model
        """
        user, c = ContributorAccessToken.objects.get_or_create(
            user=contributor
        )
        if self.OAUTH_PROVIDER == 'github':
            user.github_access_token = self.ACCESS_TOKEN
        elif self.OAUTH_PROVIDER == 'gitlab':
            user.gitlab_access_token = self.ACCESS_TOKEN
        user.save()


class MentorsList(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = Mentor.objects.all()
    serializer_class = MentorSerializer

    def get(self, request, *args, **kwargs):
        query_params = self.request.query_params
        data = MentorSerializer(self.get_queryset(), many=True).data
        if query_params:
            year = query_params.get('year')
            program = query_params.get('program')
            if year and program:
                data = self.get_filtered_results(year, program)
            else:
                data = [
                    {
                        'message': 'No records found for year-{}'
                                   ' and program-{}'.format(year, program)
                    }
                ]
        return Response(data)

    def get_filtered_results(self, year, program):
        qs = self.get_queryset()
        filtered_results = []
        for mentor in qs:
            data = MentorSerializer(mentor).data
            data['mentoring_stats'] = json.loads(data.get('mentoring_stats'))
            mentoring_stats = data['mentoring_stats']
            if program in mentoring_stats.keys():
                mentored_years = mentoring_stats[program]
                if year:
                    if int(year) in mentored_years:
                        filtered_results.append(data)
                else:
                    filtered_results.append(data)
        return filtered_results


class AssignIssue(views.APIView):
    permission_classes = []
    authentication_classes = []
    assign_issue_object = None

    def get(self, request, format=None):
        params = self.request.query_params
        # Check whether the opened url is valid or not
        is_valid = self.validate_query_search(params)

        if is_valid:
            issue = self.assign_issue()
            message = self.get_success_message(issue)
            self.assign_issue_object.delete()
        else:
            message = {
                'message': 'An invalid request was received. Please open'
                           ' the correct URL you received via an e-mail!'
            }

        return Response(message)

    def validate_query_search(self, query_params):
        """
        Validate wheter the query params in the url are valid or not! The
        query params contains the issue details that assigns the issue to
        the user. Also, the details are already being saved to the database
        :param query_params: a dict of query params
        :return: a boolean
        """
        hoster = query_params.get('hoster', None)
        repo_name = query_params.get('repo', None)
        number = query_params.get('issue', None)
        username = query_params.get('user', None)
        query_filters = {
            'username': username,
            'hoster': hoster,
            'repository_name': repo_name,
            'issue_number': int(number)
        }
        request_objs = AssignIssueRequest.objects.filter(**query_filters)

        if request_objs.exists():
            self.assign_issue_object = request_objs.first()
            return True
        else:
            return False

    def assign_issue(self):
        """
        Assign the issue to the user
        :return: an instance of igitt issue
        """
        repository = self.assign_issue_object.repository_name
        number = self.assign_issue_object.issue_number

        if self.assign_issue_object.hoster == 'github':
            user = GitHubUser(GITHUB_TOKEN, self.assign_issue_object.username)
            issue = GitHubIssue(GITHUB_TOKEN, repository, number)
        else:
            user_id = self.get_gitlab_user_id()
            user = GitLabUser(GITLAB_TOKEN, user_id)
            issue = GitLabIssue(GITLAB_TOKEN, repository, number)

        issue.assign(user)
        return issue

    def get_gitlab_user_id(self):
        """
        Get the GitLab user id
        :return: the user identifier
        """
        search_results = GitLab.get(
            GITLAB_TOKEN, GitLabMain.absolute_url(
                '/users/?username={user}'.format(
                    user=self.assign_issue_object.username
                )
            )
        )
        user = search_results.pop()
        return user['id']

    def get_success_message(self, issue):
        """
        Frame the message that the issue has been assigned to the user
        :param issue: an instance of igitt issue
        :return: the success message
        """
        issue_url = 'https://{hoster}.com/{repository}/issues/{number}'
        issue_url = issue_url.format(
            hoster=self.assign_issue_object.hoster,
            repository=self.assign_issue_object.repository_name,
            number=str(self.assign_issue_object.issue_number)
        )
        return {
            'message': 'Assigned issue {url} to {user}'.format(
                url=issue_url, user=self.assign_issue_object.username
            ),
            'issue': {
                'title': issue.title,
                'url': issue.web_url
            }
        }


class GetIssueDetails(views.APIView):

    def get(self, request, format=None):
        params = self.request.query_params
        issue_url = params.get('url', None)
        issue_data = self.get_issue_data(issue_url)
        return Response(issue_data)

    def get_issue_data(self, issue_url):
        """
        Get the issue details either from the webservices database or from the
        API.
        :param issue_url: Web URL of the Issue
        :return: a dict containing related details to issue
        """
        if issue_url:
            match = re.match(
                r'https://(github|gitlab)\.com/(.+)/([^/]+)/issues/(\d+)',
                issue_url
            )
            if match:
                resource = match.group(1)
                org_name = match.group(2)
                repo_name = match.group(3)
                issue_number = int(match.group(4))
                repo_full_name = org_name + '/' + repo_name
                issue_details = self.get_issue_details_from_db(
                    resource=resource, repo_full_name=repo_full_name,
                    issue_number=issue_number
                )
                if issue_details:
                    return issue_details
                else:
                    return self.get_issue_details_via_api(
                        resource=resource, repo_full_name=repo_full_name,
                        issue_number=issue_number
                    )
        return dict()

    def get_issue_details_from_db(self, **kwargs):
        """
        Get the Issue from the databse
        :param kwargs: Required values - Repository full name, resource (
        github or gitlab) and the issue number
        :return: the saved data of issue in the database
        """
        repos = IGittRepository.objects.filter(
            full_name=kwargs.get('repo_full_name'),
            hoster=kwargs.get('resource')
        )
        if repos:
            repo_id = repos.first().id
            filtered_issues = IGittIssue.objects.filter(
                repo_id=repo_id, number=kwargs.get('issue_number')
            )
            if filtered_issues:
                return filtered_issues.first().data
        return None

    def get_issue_details_via_api(self, **kwargs):
        """
        Get the issue details by making an API call based on the hoster
        :param kwargs: Required values - Repository full name, resource (
        github or gitlab) and the issue number
        Since, the issue doesn't exists in database, save it!
        :return: the issue data, that is saved to the database
        """
        if kwargs.get('resource') == 'github':
            issue = GitHubIssue(
                GITHUB_TOKEN, kwargs.get('repo_full_name'),
                kwargs.get('issue_number')
            )
        else:
            issue = GitLabIssue(
                GITLAB_TOKEN, kwargs.get('repo_full_name'),
                kwargs.get('issue_number')
            )
        try:
            issue.data.refresh()
            if kwargs.get('repo_full_name').__contains__(ORG_NAME):
                return self.save_data_to_database(issue, **kwargs)
        except RuntimeError:
            return {}

    def save_data_to_database(self, issue, **kwargs):
        """
        Save the Repository and Issue details to the webservices database
        :param issue: An object of GitHubIssue or GitLabIssue
        :param kwargs: Required values - Repository full name, resource (
        github or gitlab) and the issue number
        :return: the issue data saved to the database
        """
        hoster = kwargs.get('resource')
        if hoster == 'github':
            repo = GitHubRepository(
                GITHUB_TOKEN, kwargs.get('repo_full_name')
            )
        else:
            repo = GitLabRepository(
                GITLAB_TOKEN, kwargs.get('repo_full_name')
            )
        repository = save_repository(repo, hoster)
        return save_issue(hoster, repository, issue)
