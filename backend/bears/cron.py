import json
import shutil
import sys
from coalib.misc.Shell import run_shell_command
from bears.models import Bear


def fetch_bears():
    cmd = [sys.executable,
           shutil.which('coala'),
           '--json', '--show-bears', '--show-details', '-I']
    res, error = run_shell_command(cmd)
    result = json.loads(res)

    for bear in result['bears']:
        b = Bear()
        b.name = bear['name']
        b.to_json(bear)
        b.save()
