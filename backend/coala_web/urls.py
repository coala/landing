"""coala_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name=home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView

from bears.views import home, search, analytics
from org.views import (
    ContributorsAPIView,
    GetIssueDetails,
    issues,
    merge_requests,
    newcomers_active,
    org_activity,
    get_inactive_issues,
    get_unassigned_issues,
    OrganizationTeams,
    FeedbackListView,
    CalendarEventsListView,
    GSOCStudentsList,
    AddGSoCStudent,
    AccessTokenValidator,
    MentorsList,
    AssignIssue
    )
from .views import dump_database
from try_online.views import editor
from osforms.views import OSFormsListView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^list/bears', home),
    url(r'^search/bear', search),
    url(r'^contrib', ContributorsAPIView.as_view()),
    url(r'^teams', OrganizationTeams.as_view()),
    url(r'^editor/', editor),
    url(r'^analytics/', analytics),
    url(r'^feedback', FeedbackListView.as_view()),
    url(r'^osforms', OSFormsListView.as_view()),
    url(r'^calendar', CalendarEventsListView.as_view()),
    url(r'^gsoc/students', GSOCStudentsList.as_view()),
    url(r'^gsoc/student/(?P<id>[-\w\-]+)/save', AddGSoCStudent.as_view()),
    url(r'^mentors', MentorsList.as_view()),
    url(r'^issue/details', GetIssueDetails.as_view()),
    url(r'^issues/github/all', issues, {'hoster': 'github'}),
    url(r'^issues/gitlab/all', issues, {'hoster': 'gitlab'}),
    url(r'^mrs/github/all', merge_requests, {'hoster': 'github'}),
    url(r'^mrs/gitlab/all', merge_requests, {'hoster': 'gitlab'}),
    url(r'^newcomers/active', newcomers_active),
    url(r'^inactive/issues/all', get_inactive_issues),
    url(r'^unassigned/issues/all', get_unassigned_issues),
    url(r'^meta-review', include('meta_review.urls')),
    url(r'^gamification', include('gamification.urls')),
    url(r'^activity', org_activity),
    url(r'^dump/database', dump_database),
    url(r'(?P<oauth_provider>[-\w]+)/(?P<access_token>[-\w\d]+)/validate',
        AccessTokenValidator.as_view()),
    url(r'^assign', AssignIssue.as_view()),
    url(r'^', TemplateView.as_view(template_name='home.html'))
]
