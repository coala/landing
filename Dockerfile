FROM coala/base:0.9
MAINTAINER Lasse Schuirmann <lasse.schuirmann@gmail.com>

RUN mkdir /app
WORKDIR /app

# Just so it builds faster when changing app code but not requirements
RUN mkdir /app/.git
COPY .git/config /app/.git/
COPY backend/requirements.txt /app
COPY backend /app
RUN pip install -r requirements.txt

ENV BEAR_CRON_TIME='0 0,12 * * *' ORG_CRON_TIME='0 0,12 * * *'
ENV USE_DB_CACHE=True GITHUB_ISSUES_CRON_TIME='0 2,14 * * *'
ENV GITLAB_ISSUES_CRON_TIME='0 4,16 * * *' GITHUB_MRS_CRON_TIME='0 3,15 * * *'
ENV GITLAB_MRS_CRON_TIME='0 5,17 * * *' FEEDBACK_CRON_TIME='0 22 * * *'
ENV OSFORMS_DB_CRON_TIME='0 22 * * *'  CALENDAR_EVENTS_CRON_TIME='0 22 * * *'
ENV GSOC_TIMELINE_FETCH_CRON_TIME='0 0 30 11 *' GCI_TIMELINE_FETCH_CRON_TIME='0 0 1 8 *'
ENV CONTRIB_GEOLOCATION_CRON_TIME='30 22 * * *' INACTIVE_ISSUES_CRON_TIME='0 6,18 * * *'
ENV UNASSIGNED_ISSUES_CRON_TIME='0 6,18 * * *' META_REVIEW_CRON_TIME='0 6,18 * * *'
ENV ORG_ACTIVITY_CRON_TIME='0 6,18 * * *' GAMIFICATION_CRON_TIME='0 6,18 * * *'
ENV GSOC_STUDENT_CRON_TIME='0 22 * * *'
ENV  FETCH_GCI_STUDENTS_CRON_TIME='0 6,18 * * *' CONTRIBUTORS_CRON_TIME='30 22 * * *'
ENV MENTOR_CRON_TIME='30 22 * * *' JOIN_COMMUNITY_CRON_TIME='0 22 * * *'
ENV ASSIGN_ISSUES_CRON_TIME='*/180 * * * *' PROMOTE_NEWCOMERS_CRON_TIME='30 22 * * *'

RUN python3 manage.py migrate
RUN python3 manage.py createcachetable
RUN python3 manage.py crontab add
# Create config data related to gamification system
RUN python3 manage.py create_config_data
RUN python3 manage.py fetch_hosted_data

CMD gunicorn coala_web.wsgi -b 0.0.0.0:8000
